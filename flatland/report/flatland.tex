\documentclass{article}
\usepackage{enumerate}
\usepackage{alltt}
\usepackage{amsmath}
\usepackage{mathtools}
\usepackage[margin=10pt]{subcaption}
\usepackage{hyperref}
\usepackage{tikz}
\usepackage{tabularx}
\usepackage{graphicx}
%\usepackage[margin=.7in]{geometry}
\usepackage[utf8]{inputenc}
%\usepackage[T1S]{fontenc}

\setlength{\parindent}{0.0in}
\setlength{\parskip}{0.1in}
\DeclareUnicodeCharacter{00A0}{ }

\begin{document}
\begin{titlepage}
   \vspace*{\fill}
    \begin{center}
	\textsc{\LARGE IT3708 Subsymbolic Methods in AI}\\[1.5cm]
	\textsc{\Large Project 3}\\[0.5cm]
	{ \huge \bfseries Evolving Neural Networks for a Flatland Agent\\[0.4cm] }

	Martin \textsc{Gammelsæter} -- \texttt{martigam@stud.ntnu.no}
	
    \vspace*{\fill}
	{\large \today}
	\end{center}
\thispagestyle{empty}
\end{titlepage}
\setcounter{page}{1}

\section{Introduction}
This report describes the use of an evolutionary algorithm to evolve the weights
for a small artificial neural network used for controlling an agent in a
2-dimensional grid called ``Flatland'', with the goal of eating as much food
as possible, and avoiding poison. This is done to illustrate emergence of
rudimentary intelligence from artificial neural networks. As the implementation
of the EA-system has been thorougly discussed in earlier reports, this report
will focus on results.

\section{Architecture}

\subsection{EA parameters}
In Table~\ref{table:eaparams} the parameters used for the evolution of the
weights can be seen. These values are in no way claimed to be perfect for the
task, but through a process of manual hill-climbing from a starting point of
more or less sane defaults, these parameters showed fairly good results.
The hardest part of using the tournament selection parent selection mechanism is
adjusting $K$ and $\epsilon$ so as to find the balance between exploration and
exploitation.

\begin{table}[h]
  \centering
  %\resizebox{\columnwidth}{!}{%
  \begin{tabular}{l | l}
    \textsc{Parameter} & \textsc{Value} \\
    \hline
	Pool size & 150 \\
	Generations & 200 \\
	Adult selection mechanism & A-I \\
	Parent selection mechanism & Tournament selection \\
	Tournament size & 5 \\
	$\epsilon$ & 0.1 \\
	Cross-over rate & 0.5 \\
	Per-gene mutation rate & 0.01 \\	
	Elitism & 1 individual\\
  \end{tabular}
  %}
  \caption{Parameters used for successful runs of the system} 
  \label{table:eaparams}
\end{table}

\subsection{Fitness function}
The design of the fitness function was kept as simple as possible. The fitness
of a given phenotype is simply the number of food-items consumed over five
scenarios, minus the number of poison-items consumed over the same five
scenarios. The poison-factor is weighted by a constant. Mathematically, the
fitness function looks like:

\begin{equation}
	Fitness = FoodConsumed - 0.8 * PoisonConsumed
\end{equation}

This function tries to strike a balance between punishing consuming potion to
avoid it, and not punishing too much so that the agent dares to move.

\subsection{ANN design}
Just as in the fitness function, the neural network has been kept as simple as
possible. In this case, that means no bias neurons, and a very simple activation
function:

\begin{equation}
o_{i} = \left\{ 
  \begin{array}{l l}
    1 & \quad \text{if $\sum_{j=1}^{I_{i}} o_{j}w_{ij} > 0$}\\
    0 & \quad \text{else}
  \end{array} \right.
\end{equation}
where $I_{i}$ is all the inputs to neuron $i$. The weights are not capped, but
allowed to float freely. In practice this did not turn out to be a problem, and
is self corrected by the EA.

The general topology of the neural network can be seen in Fig.~\ref{fig:nn}.
On the left side are all the inputs, one for each sensor on the agent.
Signals from these pass through the three-neuron hidden layer, which pass on
signals to the ouput layer, consisting of three neurons, one for each direction
the agent can take.

To arrive at this design, I started with an as simple as possible network, and
decided to only add complexity if needed to solve the problem. As it turned out,
not much complexity was needed to achieve fairly satisfactory results.

\def\layersep{2.5cm}
\begin{figure}
\centering
\begin{tikzpicture}[shorten >=1pt,->,draw=black!50, node distance=\layersep]
    \tikzstyle{every pin edge}=[<-,shorten <=1pt]
    \tikzstyle{neuron}=[circle,fill=black!25,minimum size=17pt,inner sep=0pt]
    \tikzstyle{input neuron}=[neuron, fill=blue!50];
    \tikzstyle{output neuron}=[neuron, fill=blue!50];
    \tikzstyle{hidden neuron}=[neuron, fill=blue!50];
    \tikzstyle{annot} = [text width=4em, text centered]

    % Draw the input layer nodes
    \foreach \name / \y in {1,...,6}
    % This is the same as writing \foreach \name / \y in {1/1,2/2,3/3,4/4}
		\path[yshift=0.5cm]
        	node[input neuron, pin=left:Input] (I-\name) at (0,-\y) {};

    % Draw the hidden layer nodes
    \foreach \name / \y in {1,...,3}
        \path[yshift=0.5cm]
            node[hidden neuron] (H-\name) at (\layersep,-\y cm) {};

    % Draw the output layer node
    \foreach \name / \y in {1,...,3}
        \path[yshift=0.5cm]
            node[output neuron,pin={[pin edge={->}]right:Output}, right of=H-\name] (O-\name) at (\layersep,-\y cm) {};
    %\node[output neuron,pin={[pin edge={->}]right:Output}, right of=H-3] (O) {};

    % Connect every node in the input layer with every node in the
    % hidden layer.
    \foreach \source in {1,...,6}
        \foreach \dest in {1,...,3}
            \path (I-\source) edge (H-\dest);

    % Connect every node in the hidden layer with the output layer
    \foreach \source in {1,...,3}
        \foreach \dest in {1,...,3}
	        \path (H-\source) edge (O-\dest);

    % Annotate the layers
    \node[annot,above of=H-1, node distance=1cm] (hl) {Hidden layer};
    \node[annot,left of=hl] {Input layer};
    \node[annot,right of=hl] {Output layer};
\end{tikzpicture}
\caption{The topology of the neural network} 
\label{fig:nn}
\end{figure}

\section{Static vs. dynamic}
The difference between static and dynamic runs was that in static runs the same
five scenarios was used for every generation, while in dynamic the scenarios was
randomly generated anew for each generation. It is natural to think that the
networks in the static runs would dominate the dynamic ones in performance since
it has much longer to learn a particular environment. In reality however, the
results are very similar between the static and dynamic runs, and if anything
the dynamic ones have a slight advantage. Examples of two static and two dynamic
runs can be seen in Fig.~\ref{fig:statvsdyn}. All of these runs produce agents
that eat up all or nearly all the food, while avoiding all or all but a few
poisonous cells. Since they are both pretty equal in performance in their given
task, the obvious difference becomes the fitness plots themselves.

Because of elitism, the best individual of each generation is kept. In the
static scenario (where the fitness is calculated on the same five scenarios each
generation), this means that the max fitness always will be equal to or higher
than the preceeding generation's maximum. In the dynamic setting on the other
hand, what was great for one set of scenarios might not be as good for a newly
generated set, or the new set might be much harder or easier, resulting in a
much more unstable maximum fitness. It is probably safe to assume that the
agent produced by the dynamic version is more robust at handling new, unseen
scenarios than the static one.

\begin{figure}
\centering
\begin{subfigure}{.4\textwidth}
  \centering
  \scalebox{.4}{\input{nonrandom1}}
  \caption{Example fitness plot of a static run}
  \label{fig:nonrandom1}
\end{subfigure}
\begin{subfigure}{.4\textwidth}
  \centering
  \scalebox{.4}{\input{nonrandom2}}
  \caption{Example fitness plot of a static run}
  \label{fig:nonrandom2}
\end{subfigure}
\begin{subfigure}{.4\textwidth}
  \centering
  \scalebox{.4}{\input{random1}}
  \caption{Example fitness plot of a dynamic run}
  \label{fig:random1}
\end{subfigure}
\begin{subfigure}{.4\textwidth}
  \centering
  \scalebox{.4}{\input{random2}}
  \caption{Example fitness plot of a dynamic run}
  \label{fig:random2}
\end{subfigure}
  \caption{Different EA runs with both static and dynamic scenarios}
  \label{fig:statvsdyn}
\end{figure}

\end{document}
