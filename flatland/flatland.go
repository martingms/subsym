package main

import (
	"../lib/nn"
	"../lib/ea"
	"../lib/utils"
	"fmt"
	"runtime"
	"math/rand"
	"time"
	"bytes"
	"flag"
	"bufio"
	"os"
)

var (
	nweights = 6*6+6*3
	network *nn.NN
	scenarios []*scenario
	randomize = flag.Bool("r", false, "randomize scenarios")

	plotfile = flag.String("plotfile", "", "filename to plot to. Leave empty to not plot")
	guifile = flag.String("guifile", "run.gui", "filename for guithingie")

	mju   = flag.Int("mju", 150, "adult pool size")
	delta = flag.Int("delta", 150, "children pool size")

	K       = flag.Int("K", 5, "tournament selection group size")
	epsilon = flag.Float64("epsilon", 0.1, "1 - epsilon chance of best fit chosen")

	min = flag.Float64("min", 0.5, "min value in rank selection")
	max = flag.Float64("max", 1.5, "max value in rank selection")

	gens            = flag.Int("gens", 10000, "max generations")
	xoverRate       = flag.Float64("xr", 1.0, "crossover rate")
	mutationRate    = flag.Float64("mr", 0.01, "mutation rate")
	adultSelection  = flag.String("aselection", "A-I", "adult selection method (A-I, A-II, A-III)")
	parentSelection = flag.String("pselection", "ts", "parent selection method (fp, ss, ts, rs)")
)


func init() {
	runtime.GOMAXPROCS(runtime.NumCPU())
	rand.Seed(time.Now().UTC().UnixNano())
	flag.Parse()
}

var fo *os.File
var writer *bufio.Writer

func main() {
	// Guifile writer
	var err error
	fo, err = os.Create(*guifile)
	if err != nil {
		panic(err)
	}
	writer = bufio.NewWriter(fo)

	plot := false
	if *plotfile != "" {
		plot = true
		utils.InitFileWrite(*plotfile + ".dat")
	}

	network, _ = nn.NewNN(6, 3, []uint8{3}, false)

	population := generateGenotypes(*delta)

	earun, _ := ea.NewEa(*mju, *delta, *K, population, *epsilon, *xoverRate,
		*mutationRate, *min, *max, *adultSelection, *parentSelection, true)

	scenarios = make([]*scenario, 5)
	for i := range scenarios {
		scenarios[i] = generateScenario(0.5, 0.5)
	}

	var i int
	for i = 0; i < *gens; i++ {
		fmt.Println("Iteration:", i)
		err := earun.RunIteration()
		if err != nil { panic(err) }
		fmt.Println("Best fitness:", earun.BestFitness)
		max, avg, stddev := earun.RunStats()
		fmt.Println("Max:", max, "Avg:", avg, "Stddev:", stddev)
		//fmt.Println("Best individual:", earun.BestGenotype)

		if *randomize {
			for i := range scenarios {
				scenarios[i] = generateScenario(0.5, 0.5)
			}
		}

		if plot {
			max, avg, stddev := earun.RunStats()
			utils.Write(fmt.Sprintf("%v %v %v %v %v\n", i, max, avg, avg-stddev, avg+stddev))
		}
	}

	if plot {
		utils.FinalizeWrite()
		utils.Plot(*plotfile+".dat", *plotfile+".tex", i-1)
	}

	var winner flatlandGenotype
	if *randomize {
		winner = earun.RunBestGenotype.(flatlandGenotype)
	} else {
		winner = earun.BestGenotype.(flatlandGenotype)
	}
	network.SetWeights([][]float64{winner[0:6*6], winner[6*6:len(winner)]})
	for i, sc := range scenarios {
		food, poison := simulate(sc, true)
		fmt.Println("Sc:", i, "Food:", food, "Poison:", poison)
	}

	// GUI FILE
	writer.Flush()
	if err := fo.Close(); err != nil {
		panic(err)
	}

}

func simulate(sc *scenario, verbose bool) (int, int) {
	var totfoodc, totpoisonc int
	for t := 0; t < 50; t++ {
		if verbose {
			/*
			fmt.Println("T:", t, "===============")
			fmt.Println(sc.getSensorValues())
			fmt.Println(sc)
			fmt.Println("Food:", totfoodc, "Poison:", totpoisonc)
			*/
			writer.WriteString(sc.String())
		}
		foodc, poisonc := sc.update(network.Run(sc.getSensorValues()))
		totfoodc += foodc
		totpoisonc += poisonc
	}

	return totfoodc, totpoisonc
}

func generateScenario(food, poison float64) *scenario {
	sc := new(scenario)
	sc.board = make([][]string, 8)

	/* Fill board */
	empty := 0
	for i := range sc.board {
		sc.board[i] = make([]string, 8)
		for j := range sc.board[i] {
			if rand.Float64() < food {
				sc.board[i][j] = " F "
			} else if rand.Float64() < poison {
				sc.board[i][j] = " P "
			} else {
				sc.board[i][j] = "   "
				empty++
			}
		}
	}

	/* Choose start location */
	loc := rand.Intn(empty)
	for x, line := range sc.board {
		for y, pos := range line {
			if pos == "   " {
				if loc == 0 {
					sc.locX = x
					sc.locY = y
					sc.orientation = getRandomOrientation()
				}
				loc--
			}
		}
	}

	return sc
}

func getRandomOrientation() string {
	return []string{" ^ ", " v ", " < ", " > "}[rand.Intn(4)]
}

type scenario struct {
	board [][]string
	locX int
	locY int
	orientation string
}

func (sc *scenario) getSensorValues() []float64 {
	switch sc.orientation {
	case " ^ ":
		pos := []int{sc.locX-1, sc.locY, sc.locX, sc.locY-1, sc.locX, sc.locY+1}
		if sc.locX == 0 {
			pos[0] = 7
		}

		if sc.locY == 0 {
			pos[3] = 7
		}

		if sc.locY == 7 {
			pos[5] = 0
		}

		return sc.getSensorValueFor(pos)

	case " v ":
		pos := []int{sc.locX+1, sc.locY, sc.locX, sc.locY+1, sc.locX, sc.locY-1}
		if sc.locX == 7 {
			pos[0] = 0
		}

		if sc.locY == 0 {
			pos[5] = 7
		}

		if sc.locY == 7 {
			pos[3] = 0
		}

		return sc.getSensorValueFor(pos)
	case " < ":
		pos := []int{sc.locX, sc.locY-1, sc.locX+1, sc.locY, sc.locX-1, sc.locY}
		if sc.locY == 0 {
			pos[1] = 7
		}

		if sc.locX == 7 {
			pos[2] = 0
		}

		if sc.locX == 0 {
			pos[4] = 7
		}

		return sc.getSensorValueFor(pos)
	case " > ":
		pos := []int{sc.locX, sc.locY+1, sc.locX-1, sc.locY, sc.locX+1, sc.locY}
		if sc.locY == 7 {
			pos[1] = 0
		}

		if sc.locX == 0 {
			pos[2] = 0
		}

		if sc.locX == 7 {
			pos[4] = 0
		}

		return sc.getSensorValueFor(pos)
	default:
		panic("unknown orientation: " + sc.orientation)
	}
}

func (sc *scenario) getSensorValueFor(pos []int) []float64 {
	out := make([]float64, 6)

	if sc.board[pos[0]][pos[1]] == " P " {
		out[1], out[4] = 0, 1
	} else if sc.board[pos[0]][pos[1]] == " F " {
		out[1], out[4] = 1, 0
	} else {
		out[1], out[4] = 0, 0
	}

	if sc.board[pos[2]][pos[3]] == " P " {
		out[0], out[3] = 0, 1
	} else if sc.board[pos[2]][pos[3]] == " F " {
		out[0], out[3] = 1, 0
	} else {
		out[0], out[3] = 0, 0
	}

	if sc.board[pos[4]][pos[5]] == " P " {
		out[2], out[5] = 0, 1
	} else if sc.board[pos[4]][pos[5]] == " F " {
		out[2], out[5] = 1, 0
	} else {
		out[2], out[5] = 0, 0
	}

	return out
}

func (sc *scenario) update(outputs []float64) (int, int) {
	/* 0 = left, 1 = forward, 2 = right */
	max := outputs[0]
	besti := 0
	for i := 1; i < len(outputs); i++ {
		if outputs[i] > max {
			max = outputs[i]
			besti = i
		}
	}

	minusx := func() {
		if sc.locX == 0 {
			sc.locX = 7
		} else {
			sc.locX--
		}
	}
	plusx := func() {
		if sc.locX == 7 {
			sc.locX = 0
		} else {
			sc.locX++
		}
	}
	minusy := func() {
		if sc.locY == 0 {
			sc.locY = 7
		} else {
			sc.locY--
		}
	}
	plusy := func() {
		if sc.locY == 7 {
			sc.locY = 0
		} else {
			sc.locY++
		}
	}

	switch sc.orientation {
	case " ^ ":
		switch besti {
		case 0:
			minusy()
			sc.orientation = " < "
		case 1:
			minusx()
		case 2:
			plusy()
			sc.orientation = " > "
		}
	case " v ":
		switch besti {
		case 0:
			plusy()
			sc.orientation = " > "
		case 1:
			plusx()
		case 2:
			minusy()
			sc.orientation = " < "
		}
	case " < ":
		switch besti {
		case 0:
			plusx()
			sc.orientation = " v "
		case 1:
			minusy()
		case 2:
			minusx()
			sc.orientation = " ^ "
		}
	case " > ":
		switch besti {
		case 0:
			minusx()
			sc.orientation = " ^ "
		case 1:
			plusy()
		case 2:
			plusx()
			sc.orientation = " v "
		}
	default:
		panic("unknown orientation:" + sc.orientation)
	}

	newpos := sc.board[sc.locX][sc.locY]
	if newpos == " F " {
		sc.board[sc.locX][sc.locY] = "   "
		return 1, 0
	} else if newpos == " P " {
		sc.board[sc.locX][sc.locY] = "   "
		return 0, 1
	}

	return 0, 0
}

func (sc scenario) String() string {
	var buf bytes.Buffer

	buf.WriteString("+------------------------+\n")
	for x, line := range sc.board {
		buf.WriteString("|")
		for y, pos := range line {
			if x == sc.locX && y == sc.locY {
				buf.WriteString(sc.orientation)
			} else {
				buf.WriteString(pos)
			}
		}
		buf.WriteString("|\n")
	}
	buf.WriteString("+------------------------+\n")

	return buf.String()
}

func generateGenotypes(popsize int) []ea.Genotype {
	ret := make([]ea.Genotype, popsize)

	for i := range ret {
		individual := make(flatlandGenotype, nweights)
		for j := range individual {
			individual[j] = (rand.Float64() * 20.0) - 10.0
		}
		ret[i] = individual
	}

	return ret
}

type flatlandGenotype []float64

func (s flatlandGenotype) GeneratePhenotype() ea.Phenotype {
	return flatlandPhenotype(s)
}

func (s flatlandGenotype) CrossoverWith(mate ea.Genotype) (ea.Genotype, ea.Genotype) {
	pivot := rand.Intn(nweights)
	kid1 := make(flatlandGenotype, nweights)
	kid2 := make(flatlandGenotype, nweights)

	i := 0
	for ; i < pivot; i++ {
		kid1[i] = s[i]
		kid2[i] = mate.GetIthGene(i)
	}

	for ; i < nweights; i++ {
		kid1[i] = mate.GetIthGene(i)
		kid2[i] = s[i]
	}

	return kid1, kid2
}

func (s flatlandGenotype) Mutate(rate float64) {
	for i := range s {
		if rate > rand.Float64() {
			//s[i] += (rand.Float64()*2.0) - 1.0
			s[i] += (rand.Float64()*20.0) - 10.0
			//s[i] = -s[i]
		}
	}
}

func (s flatlandGenotype) GetIthGene(i int) float64 {
	return s[i]
}

func (s flatlandGenotype) Copy() ea.Genotype {
	out := make(flatlandGenotype, len(s))
	copy(out, s)

	return out
}

type flatlandPhenotype []float64

func (s flatlandPhenotype) Fitness() float64 {
	network.SetWeights([][]float64{s[0:6*6], s[6*6:len(s)]})

	var totfood, totpoison int
	scenarioscopy := deepcopyScenarios(scenarios)
	for _, sc := range scenarioscopy {
		food, poison := simulate(sc, false)
		totfood += food
		totpoison += poison
	}

	return float64(totfood) - 0.8 * float64(totpoison)
}

func deepcopyScenarios(scenarios []*scenario) []*scenario {
	out := make([]*scenario, len(scenarios))

	for i := range scenarios {
		out[i] = &scenario{locY: scenarios[i].locY,
					locX: scenarios[i].locX,
					orientation: scenarios[i].orientation}
		out[i].board = make([][]string, len(scenarios[i].board))
		for j := range out[i].board {
			out[i].board[j] = make([]string, len(scenarios[i].board[j]))
			copy(out[i].board[j], scenarios[i].board[j])
		}
	}

	return out
}
