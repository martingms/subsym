package ea

import (
	"../utils"
	"errors"
	"math"
	"math/rand"
	"sort"

//	"fmt"
)

// TODO: Better name
type EaRun struct {
	mju            int
	population     Population
	delta          int
	kids           Population
	BestGenotype   Genotype
	BestFitness    float64
	RunBestGenotype Genotype
	RunBestFitness float64

	xoverRate    float64
	mutationRate float64

	K       int
	epsilon float64

	min float64
	max float64

	adultSelection  string
	parentSelection string

	elitism bool
}

func NewEa(mju, delta, K int, population []Genotype, epsilon, xoverRate, mutationRate, min, max float64,
	adultSelection, parentSelection string, elitism bool) (*EaRun, error) {

	if xoverRate > 1.0 || mutationRate > 1.0 {
		return nil, errors.New("Crossover or mutation rates greater than 1.0")
	}

	/*
		if parentSelection yadda yadda... TODO
	*/

	pop := make(Population, mju)
	for i := range pop {
		pop[i] = &Individual{fitness: -1}
	}

	kids := make(Population, delta)
	for i := range kids {
		kids[i] = &Individual{geno: population[i]}
	}

	return &EaRun{
		mju:             mju,
		population:      pop,
		delta:           delta,
		kids:            kids,
		xoverRate:       xoverRate,
		mutationRate:    mutationRate,
		K:               K,
		epsilon:         epsilon,
		min:             min,
		max:             max,
		adultSelection:  adultSelection,
		parentSelection: parentSelection,
		elitism: 		 elitism,
	}, nil
}

func (ea *EaRun) RunIteration() error {
	// Fitness testing of the next generation
	ea.RunBestFitness = -10000.0
	ea.BestFitness = -10000.0
	for _, individual := range ea.kids {
		individual.pheno = individual.geno.GeneratePhenotype()
		individual.fitness = individual.pheno.Fitness()
		if individual.fitness > ea.RunBestFitness {
			ea.RunBestFitness = individual.fitness
			ea.RunBestGenotype = individual.geno.Copy()
		}
		if individual.fitness > ea.BestFitness {
			ea.BestFitness = individual.fitness
			ea.BestGenotype = individual.geno.Copy()
		}
	}

	// TODO: Remove all logging from lib (or put behind verbosity flag).
	utils.Logln("Run best fitness:", ea.RunBestFitness)

	// Adult selection
	switch ea.adultSelection {
	case "A-I":
		if len(ea.population) != len(ea.kids) {
			return errors.New("With A-I adult selection mju and delta must be equal!")
		}
		copy(ea.population, ea.kids)

	// TODO/FIXME: Do roulette-wheel selection?
	case "A-II":
		sort.Sort(ea.kids)
		copy(ea.population, ea.kids)
	// TODO/FIXME: Do roulette-wheel selection?
	case "A-III":
		popcopy := make(Population, len(ea.population))
		copy(popcopy, ea.population)
		sort.Sort(ea.kids)
		sort.Sort(popcopy)
		var kid, adult int
		for i := range ea.population {
			if ea.kids[kid].fitness > popcopy[adult].fitness {
				ea.population[i] = ea.kids[kid]
				kid++
			} else {
				ea.population[i] = popcopy[adult]
				adult++
			}
		}
	default:
		return errors.New("Unrecognizable adult selection method: " + ea.adultSelection)
	}

	var best *Individual
	if ea.elitism {
		sort.Sort(ea.population)
		best = &Individual{geno: ea.population[0].geno.Copy()}
	}

	// Parent selection
	// Reproduction
	switch ea.parentSelection {
	case "fp":
		ea.fitnessProportionateSelection()
	case "ss":
		ea.sigmaScalingSelection()
	case "ts":
		ea.tournamentSelection()
	case "rs":
		ea.rankSelection()
	default:
		return errors.New("Unrecognizable parent selection method: " + ea.parentSelection)
	}

	if ea.elitism {
		ea.kids[len(ea.kids)-1] = best
	}

	return nil
}

func (ea *EaRun) fitnessProportionateSelection() {
	totalFitness := 0.0
	for _, adult := range ea.population {
		totalFitness += adult.fitness
	}

	// TODO: Not very elegant is it?
	var parents Population
	if ea.xoverRate > 0.0 {
		parents = make(Population, 2)
	} else {
		parents = make(Population, 1)
	}

	for i := 0; i < ea.delta; i++ {
		for j := range parents {
			roulette := rand.Float64()

			sum := 0.0
			for _, adult := range ea.population {
				normalizedFitness := float64(adult.fitness) / totalFitness
				sum += normalizedFitness

				if sum > roulette {
					parents[j] = adult
					break
				}
			}
		}

		offspring := ea.crossoverAndMutate(parents)
		ea.kids[i] = offspring[0]
		if len(offspring) == 2 {
			i++
			ea.kids[i] = offspring[1]
		}
	}
}

type scaledRoulette struct {
	i   *Individual
	val float64
}

func (ea *EaRun) sigmaScalingSelection() {
	avg, stdDev := ea.population.avgStdDevFitness()
	roulettewheel := make([]scaledRoulette, len(ea.population))
	total := 0.0
	for i, adult := range ea.population {
		var expVal float64
		if stdDev == 0.0 {
			expVal = 1.0
		} else {
			expVal = 1.0 + (adult.fitness-avg)/(2*stdDev)
		}
		total += expVal
		roulettewheel[i] = scaledRoulette{i: adult, val: expVal}
	}

	// FIXME/TODO: Identical to all other rouletteselections... fix!
	var parents Population
	if ea.xoverRate > 0.0 {
		parents = make(Population, 2)
	} else {
		parents = make(Population, 1)
	}

	for i := 0; i < ea.delta; i++ {
		for j := range parents {
			roulette := rand.Float64()

			sum := 0.0
			for _, adult := range roulettewheel {
				normalizedFitness := adult.val / total
				sum += normalizedFitness

				if sum > roulette {
					parents[j] = adult.i
					break
				}
			}
		}

		offspring := ea.crossoverAndMutate(parents)
		ea.kids[i] = offspring[0]
		if len(offspring) == 2 {
			i++
			ea.kids[i] = offspring[1]
		}
	}
}

func (ea *EaRun) tournamentSelection() {
	// FIXME/TODO: Identical to all other rouletteselections... fix!
	var parents Population
	if ea.xoverRate > 0.0 {
		parents = make(Population, 2)
	} else {
		parents = make(Population, 1)
	}

	for i := 0; i < ea.delta; i++ {
		for j := range parents {
			lineup := takeN(ea.K, ea.population)
			roulette := rand.Float64()

			if roulette < (1.0 - ea.epsilon) {
				parents[j] = lineup.best()
			} else {
				parents[j] = lineup[rand.Intn(len(lineup))]
			}
		}

		offspring := ea.crossoverAndMutate(parents)
		ea.kids[i] = offspring[0]
		if len(offspring) == 2 {
			i++
			ea.kids[i] = offspring[1]
		}
	}
}

func takeN(n int, pop Population) Population {
	taken := map[int]bool{}
	result := make(Population, n)

	for i := 0; i < n; {
		random := rand.Intn(len(pop))
		if _, ok := taken[random]; !ok {
			result[i] = pop[random]
			i++
		}
	}

	return result
}

func (ea *EaRun) rankSelection() {
	roulettewheel := make([]scaledRoulette, len(ea.population))
	total := 0.0
	sort.Sort(sort.Reverse(ea.population))
	for i, adult := range ea.population {
		expVal := ea.min + (ea.max-ea.min)*(float64(i-1)/float64(len(ea.population)-1))
		total += expVal
		roulettewheel[i] = scaledRoulette{i: adult, val: expVal}
	}

	// FIXME/TODO: Identical to all other rouletteselections... fix!
	var parents Population
	if ea.xoverRate > 0.0 {
		parents = make(Population, 2)
	} else {
		parents = make(Population, 1)
	}

	for i := 0; i < ea.delta; i++ {
		for j := range parents {
			roulette := rand.Float64()

			sum := 0.0
			for _, adult := range roulettewheel {
				normalizedFitness := adult.val / total
				sum += normalizedFitness

				if sum > roulette {
					parents[j] = adult.i
					break
				}
			}
		}

		offspring := ea.crossoverAndMutate(parents)
		ea.kids[i] = offspring[0]
		if len(offspring) == 2 {
			i++
			ea.kids[i] = offspring[1]
		}
	}
}

func (ea *EaRun) crossoverAndMutate(parents Population) Population {
	offspring := make(Population, len(parents))
	if ea.xoverRate > rand.Float64() {
		// Fucken ugly, rewrite bitches.
		kid1, kid2 := parents[0].geno.CrossoverWith(parents[1].geno)
		offspring[0] = &Individual{geno: kid1}
		offspring[1] = &Individual{geno: kid2}
	} else {
		copy(offspring, parents) // Needed?
	}

	for _, kid := range offspring {
		// FUGLY: FIXME
		kid.geno.Mutate(ea.mutationRate)
	}

	return offspring
}

func (ea *EaRun) RunStats() (float64, float64, float64) {
	max := -10000.0
	for _, individual := range ea.population {
		if individual.fitness > max {
			max = individual.fitness
		}
	}

	avg, stddev := ea.population.avgStdDevFitness()

	return max, avg, stddev
}

type Population []*Individual

func (p Population) best() *Individual {
	best := 0.0
	bestIndex := 0
	for i, individual := range p {
		if individual.fitness > best {
			best = individual.fitness
			bestIndex = i
		}
	}

	return p[bestIndex]
}

func (p Population) avgStdDevFitness() (float64, float64) {
	sum := 0.0
	for _, i := range p {
		sum += i.fitness
	}
	avg := sum / float64(len(p))

	diffSum := 0.0
	for _, i := range p {
		diffSum += math.Pow(float64(i.fitness)-avg, 2)
	}

	return avg, math.Sqrt(diffSum / float64(len(p)))
}

func (p Population) Len() int      { return len(p) }
func (p Population) Swap(i, j int) { p[i], p[j] = p[j], p[i] }

// Note, Less is More ;)
func (p Population) Less(i, j int) bool { return p[i].fitness > p[j].fitness }

type Individual struct {
	geno    Genotype
	pheno   Phenotype
	fitness float64
}

type Genotype interface {
	CrossoverWith(Genotype) (Genotype, Genotype)
	GetIthGene(int) float64
	Mutate(float64)
	GeneratePhenotype() Phenotype
	Copy() Genotype
}

type Phenotype interface {
	Fitness() float64
}
