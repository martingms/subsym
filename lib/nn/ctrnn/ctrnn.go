package ctrnn

import (
	"math"
	"math/rand"
	"bytes"
	"fmt"
)

type Network struct {
	neurons []*neuron
	inputs int
	outputs int
	hiddenlayers []int
}

func NewCTRNN(inputs, outputs int, hiddenlayers []int) *Network {
	nn := new(Network)
	nn.inputs, nn.outputs = inputs, outputs
	nn.hiddenlayers = make([]int, len(hiddenlayers))
	copy(nn.hiddenlayers, hiddenlayers)

	nneurons := inputs + outputs
	for _, n := range hiddenlayers {
		nneurons += n
	}
	nn.neurons = make([]*neuron, nneurons)

	/* Input layer */
	i := 0
	sum := inputs
	for ; i < sum; i++ {
		nn.neurons[i] = &neuron{network: nn, timec: 1.0, gain: 1.0}
	}

	/* Hidden layers */
	lastn := inputs
	for _, ln := range hiddenlayers {
		sum += ln
		for ; i < sum; i++ {
			nn.neurons[i] = &neuron{inputs: make(map[int]float64), network: nn, timec: 1.0, gain: 1.0}
			for j := sum-ln-lastn; j < sum; j++ {
				nn.neurons[i].inputs[j] = (rand.Float64() * 20.0) - 10.0 // TODO: Tweak
			}
		}
		lastn = ln
	}

	/* Output layer */
	sum += outputs
	for ; i < sum; i++ {
		nn.neurons[i] = &neuron{inputs: make(map[int]float64), network: nn, timec: 1.0, gain: 1.0}
		for j := sum-outputs-lastn; j < sum; j++ {
			nn.neurons[i].inputs[j] = (rand.Float64() * 20.0) - 10.0 // TODO: Tweak
		}
	}

	return nn
}

func (nn *Network) Run(inputs []float64) []float64 {
	/* Input layer */
	i := 0
	sum := nn.inputs
	for ; i < sum; i++ {
		//nn.neurons[i].external = inputs[i]
		//nn.neurons[i].tick()
		//nn.neurons[i].output = nn.neurons[i].o
		nn.neurons[i].output = inputs[i]
	}

	/* Hidden layers */
	//fmt.Println("hiddenlayers", nn.hiddenlayers)
	for _, ln := range nn.hiddenlayers {
		sum += ln
		for ; i < sum; i++ {
			nn.neurons[i].tick()
		}

		i -= ln
		for ; i < sum; i++ {
			nn.neurons[i].output = nn.neurons[i].o
		}
	}

	/* Output layer */
	sum += nn.outputs
	for ; i < sum; i++ {
		nn.neurons[i].tick()
	}

	out := make([]float64, nn.outputs)
	for i := range out {
		out[i] = nn.neurons[len(nn.neurons)-1-i].o
	}
	/*
	for i := 0; i < nn.inputs; i++ {
		nn.neurons[i].external = inputs[i]
	}

	for i := range nn.neurons {
		nn.neurons[i].tick()
	}

	for i := range nn.neurons {
		nn.neurons[i].output = nn.neurons[i].o
	}

	out := make([]float64, nn.outputs)
	for i := range out {
		out[i] = nn.neurons[len(nn.neurons)-1-i].output
	}
	*/

	return out
}

func (nn *Network) SetWeights(weights []float64) {
	var totweights int
	for i, n := range nn.neurons {
		if len(n.inputs) != 0 {
			for j := range n.inputs {
				nn.neurons[i].inputs[j] = weights[totweights]
				totweights++
			}
		}
	}
}

func (nn *Network) SetVariables(bias, gain, timec []float64) {
	for i := range nn.neurons {
		nn.neurons[i].bias = bias[i]
		nn.neurons[i].gain = gain[i]
		nn.neurons[i].timec = timec[i]
	}
}

func (nn *Network) Reset() {
	for i := range nn.neurons {
		nn.neurons[i].s = 0
		nn.neurons[i].external = 0
		nn.neurons[i].y = 0
		nn.neurons[i].output = 0
	}
}

func (nn Network) String() string {
	var buf bytes.Buffer

	for i, neuron := range nn.neurons {
		buf.WriteString(fmt.Sprintf("%v (s: %v, gain: %v, timec: %v, y: %v, bias: %v, input: %v, output: %v, ninputs: %v)\n",
			i, neuron.s, neuron.gain, neuron.timec, neuron.y, neuron.bias, neuron.external, neuron.output, len(neuron.inputs)))

		for j, w := range neuron.inputs {
			buf.WriteString(fmt.Sprintf("	%v: %v\n", j, w))
		}
	}

	return buf.String()
}

type neuron struct {
	s float64
	external float64
	gain float64
	timec float64
	y float64
	o float64
	output float64
	bias float64
	inputs map[int]float64
	network *Network
}

func (n *neuron) tick() {
	n.s = n.external

	for i, weight := range n.inputs {
		n.s += n.network.neurons[i].output * weight
	}

	n.y += (-n.y + n.s + n.bias) / n.timec

	n.o = 1.0 / (1.0 + math.Exp(-n.gain * n.y))
}
