package nn

import (
	"math/rand"
	//"fmt"
)

type NN struct {
	weights [][]float64
	biasweights []float64
}

func NewNN(inputs, outputs uint8, hiddenlayers []uint8, bias bool) (*NN, error) {
	nn := new(NN)

	/* Initialize weight slices */
	nn.weights = make([][]float64, len(hiddenlayers)+1)
	nn.weights[0] = make([]float64, inputs*hiddenlayers[0])
	for i := 1; i < len(hiddenlayers); i++ {
		nn.weights[i] = make([]float64, hiddenlayers[i-1]*hiddenlayers[i])
	}
	nn.weights[len(nn.weights)-1] = make([]float64, hiddenlayers[len(hiddenlayers)-1]*outputs)

	// FIXME: Can be done "inline" for performance..
	for _, layer := range nn.weights {
		for i := range layer {
			layer[i] = initialWeight()
		}
	}

	if bias {
		/* Counting all neurons, excluding inputs */
		var neurons uint8
		for _, layer := range hiddenlayers {
			neurons += layer
		}
		neurons += outputs

		nn.biasweights = make([]float64, neurons)
		for i := range nn.biasweights {
			nn.biasweights[i] = initialWeight()
		}
	}

	return nn, nil
}

func (nn *NN) SetWeights(weights [][]float64) {
	var i int
	for i = 0; i < len(nn.weights); i++ {
		copy(nn.weights[i], weights[i])
	}

	/* If bias weights supplied */
	if i < len(weights)-1 {
		copy(nn.biasweights, weights[len(weights)-1])
	}
}

func (nn *NN) Run(inputs []float64) []float64 {
	values := make([]float64, len(inputs))
	copy(values, inputs)
	// TODO: Copy inputs before editing?
	for _, layer := range nn.weights {
		activation := make([]float64, len(layer)/len(values))
		for j := range activation {
			curweights := layer[j*len(values):j*len(values)+len(values)]
			activation[j] = nn.activationFunction(values, curweights) //TODO: Add bias
		}
		values = make([]float64, len(activation))
		copy(values, activation)
	}

	return values
}

// TODO: Add different types.
func (nn *NN) activationFunction(input, weights []float64) float64 {
	assert("len(input) == len(weights)", len(input) == len(weights))

	sum := 0.0
	for i, x := range input {
		sum += x * weights[i]
	}

	if sum > 0.0 {
		return 1.0
	} else {
		return 0.0
	}
}

func initialWeight() float64 {
	// TODO: Should initial weights be customizable?
	return (rand.Float64() * 2.0) - 1.0
}

// FIXME: Lolhack
func assert(what string, x bool) {
	if !x {
		panic("assertion \"" + what + "\" fails.")
	}
}
