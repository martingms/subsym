package utils

import (
	"bufio"
	"fmt"
	"os"

//	"os/exec"
)

// TODO/FIXME FIX VERBOSITY

func Log(a ...interface{}) {
	//if *v {
	fmt.Print(a...)
	//}
}

func Logln(a ...interface{}) {
	//if *v {
	fmt.Println(a...)
	//}
}

var fo *os.File
var writer *bufio.Writer

func InitFileWrite(filename string) {
	var err error
	fo, err = os.Create(filename)
	if err != nil {
		panic(err)
	}

	writer = bufio.NewWriter(fo)
}

func Write(str string) {
	writer.WriteString(str)
	writer.Flush()
}

func FinalizeWrite() {
	writer.Flush()
	if err := fo.Close(); err != nil {
		panic(err)
	}
}

func Plot(datfile, plotfile string, maxX int) {
	f, err := os.Create("plot.gp")
	if err != nil {
		panic(err)
	}

	w := bufio.NewWriter(f)

	w.WriteString("set terminal epslatex\n")
	w.WriteString(fmt.Sprintf("set output '%v'\n", plotfile))
	w.WriteString("set key noautotitle inside right bottom vertical width -8\n")
	w.WriteString("set xlabel \"Generation\"\n")
	w.WriteString("set ylabel \"Fitness\"\n")
	w.WriteString(fmt.Sprintf("set xrange [0:%v]\n", maxX))
	w.WriteString("set xtics scale 0\n")
	w.WriteString("set ytics scale 0\n")

	w.WriteString(fmt.Sprintf("plot '%v' using 1:4:5 title 'Fitness std.dev.' with filledcurves lc rgb '#c3d4d9' lt 1, ", datfile))
	w.WriteString("'' using 1:3 title 'Fitness average' with lines lc rgb '#7999b5' lw 3 lt 1, ")
	w.WriteString("'' using 1:2 title 'Max. fitness' with lines lc rgb '#464e4e' lw 3 lt 1;\n")
	w.Flush()
	f.Close()

	//err = exec.Command(fmt.Sprintf("/usr/bin/gnuplot %v", tmpfile)).Run()
	//if err != nil { panic(err) }
}

func Min(x, y int) int {
	if x < y {
		return x
	} else {
		return y
	}
}

func Max(x, y int) int {
	if x > y {
		return x
	} else {
		return y
	}
}

func MaxIntSlice(x []int) int {
	max := x[0]
	for _, n:= range x {
		if n > max {
			max = n
		}
	}

	return max
}

func Round(x float64) float64 {
	low := float64(int(x))
	if x - low >= 0.5 {
		fmt.Println(x)
		return x+1.0
	}

	return low
}

func PanicIfErr(err error) {
	if err != nil {
		panic(err)
	}
}
