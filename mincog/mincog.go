package main

import (
	"runtime"
	"math/rand"
	"fmt"
	"os"
	"bufio"
	"time"
	"bytes"
	"math"
	"flag"
	"../lib/utils"
	"../lib/nn/ctrnn"
	"../lib/ea"
)

var (
	network *ctrnn.Network
	curstate *state
	ngenes = (7+7+4+4+9*3)*8
	plotfile = flag.String("plotfile", "", "filename to plot to. Leave empty to not plot")
	guifile = flag.String("guifile", "run.gui", "filename for guithingie")

	mju   = flag.Int("mju", 100, "adult pool size")
	delta = flag.Int("delta", 100, "children pool size")

	K       = flag.Int("K", 5, "tournament selection group size")
	epsilon = flag.Float64("epsilon", 0.1, "1 - epsilon chance of best fit chosen")

	min = flag.Float64("min", 0.7, "min value in rank selection")
	max = flag.Float64("max", 1.4, "max value in rank selection")

	gens            = flag.Int("gens", 200, "max generations")
	xoverRate       = flag.Float64("xr", 0.99, "crossover rate")
	mutationRate    = flag.Float64("mr", 0.001, "mutation rate")
	adultSelection  = flag.String("aselection", "A-I", "adult selection method (A-I, A-II, A-III)")
	parentSelection = flag.String("pselection", "rs", "parent selection method (fp, ss, ts, rs)")
)

func init() {
	runtime.GOMAXPROCS(runtime.NumCPU())
	rand.Seed(time.Now().UTC().UnixNano())
	flag.Parse()
}

var fo *os.File
var writer *bufio.Writer

func main() {
	// Guifile writer
	var err error
	fo, err = os.Create(*guifile)
	if err != nil {
		panic(err)
	}
	writer = bufio.NewWriter(fo)

	plot := false
	if *plotfile != "" {
		plot = true
		utils.InitFileWrite(*plotfile + ".dat")
	}

	network = ctrnn.NewCTRNN(5, 2, []int{2})
	curstate = generateState(40, 15)

	population := generateGenotypes(*delta)

	earun, _ := ea.NewEa(*mju, *delta, *K, population, *epsilon, *xoverRate,
		*mutationRate, *min, *max, *adultSelection, *parentSelection, true)

	var i int
	for i = 0; i < *gens; i++ {
		fmt.Println("Iteration:", i)
		err := earun.RunIteration()
		if err != nil { panic(err) }
		fmt.Println("Best fitness:", earun.BestFitness)
		max, avg, stddev := earun.RunStats()
		fmt.Println("Max:", max, "Avg:", avg, "Stddev:", stddev)
		//curstate = generateState(40, 15)

		if plot {
			utils.Write(fmt.Sprintf("%v %v %v %v %v\n", i, max, avg, avg-stddev, avg+stddev))
		}
	}

	if plot {
		utils.FinalizeWrite()
		utils.Plot(*plotfile+".dat", *plotfile+".tex", i-1)
	}

	winner := earun.BestGenotype.(mincogGenotype)
	winnerpheno := winner.GeneratePhenotype().(mincogPhenotype)
	winnerpheno.updateNetwork()

	st := curstate.deepCopy()
	st.simulate(true)
	fmt.Println("FITNESS:", winnerpheno.Fitness())

	// GUI FILE
	writer.Flush()
	if err := fo.Close(); err != nil {
		panic(err)
	}
}

type state struct {
	agent paddle
	curobj int
	objects []object
	xmax int
	ymax int
}

func generateState(x, y int) *state {
	st := state{xmax: x, ymax: y}
	st.agent = paddle{size: 5, offset: (x/2)-3}

	st.objects = make([]object, 40)
	for i := range st.objects {
		st.objects[i] = generateObject()
	}

	return &st
}

func (st *state) deepCopy() *state {
	out := &state{ymax: st.ymax, xmax: st.xmax, curobj: st.curobj, agent: st.agent}
	out.objects = make([]object, len(st.objects))
	copy(out.objects, st.objects)

	return out
}

func (st *state) simulate(verbose bool) (capture, collision, avoidance int) {
	curst := st.deepCopy()

	for i := 0; i < len(curst.objects); i++ {
		var sumOn, sumOff int
		for j := 0; j < curst.ymax-1; j++ {
			vals := network.Run(curst.getSensorValues())
			sumOn, sumOff = curst.step(interpretOutput(vals), verbose)
			if verbose {
				fmt.Println(vals)
				fmt.Println("Delta:", interpretOutput(vals))
			}

			if sumOn + sumOff == 0 {
				continue
			}

			if sumOn > 0 && sumOff == 0 {
				if verbose {
					fmt.Println("Capture!")
				}
				capture++
			} else if sumOn > 0 && sumOff > 0 {
				if verbose {
					fmt.Println("Collision!")
				}
				collision++
			} else if sumOn == 0 && sumOff > 0 {
				if verbose {
					fmt.Println("Avoidance!")
				}
				avoidance++
			}

		}
	}

	if verbose {
		fmt.Println("Score:", capture, "captures,", collision, "collisions,", avoidance, "avoidances")
	}

	return
}

func (st *state) step(delta int, verbose bool) (int, int) {
	if verbose {
		fmt.Println(st)
		writer.WriteString(st.String())
	}

	st.objects[st.curobj].line++
	st.objects[st.curobj].offset += st.objects[st.curobj].direction

	if st.objects[st.curobj].offset >= st.xmax {
		st.objects[st.curobj].offset -= st.xmax
	} else if st.objects[st.curobj].offset < 0 {
		st.objects[st.curobj].offset = st.xmax + st.objects[st.curobj].offset
	}

	st.agent.offset += delta
	if st.agent.offset >= st.xmax {
		st.agent.offset -= st.xmax
	} else if st.agent.offset < 0 {
		st.agent.offset = st.xmax + st.agent.offset
	}


	if st.objects[st.curobj].line == st.ymax-1 {
		var sumOn, sumOff int
		for i := 0; i < st.xmax; i++ {
			if st.onAgent(i) && st.onObj(i) {
				sumOn++
			} else if st.onObj(i) {
				sumOff++
			}
		}

		if verbose {
			fmt.Println(st)
			writer.WriteString(st.String())
		}

		st.curobj++
		return sumOn, sumOff
	}

	return 0, 0
}

func (st *state) getSensorValues() []float64 {
	out := make([]float64, 5)

	for i := range out {
		if st.onObj(i + st.agent.offset) {
			out[i] = 1.0
		} else {
			out[i] = 0.0
		}
	}

	return out
}

func (st state) String() string {
	var buf bytes.Buffer

	// TODO: This (and more further down) should be st.xmax*"-"
	buf.WriteString("+----------------------------------------+\n")

	for i := 0; i < st.ymax; i++ {
		buf.WriteString("|")
		if i == st.objects[st.curobj].line && i != st.ymax-1 {
			for j := 0; j < st.xmax; j++ {
				if st.onObj(j) {
					buf.WriteString("*")
				} else {
					buf.WriteString(" ")
				}
			}
		} else if i != st.objects[st.curobj].line && i == st.ymax-1 {
			for j := 0; j < st.xmax; j++ {
				if st.onAgent(j) && st.onObj(j) {
					buf.WriteString("-")
				} else if st.onAgent(j) {
					buf.WriteString("#")
				} else {
					buf.WriteString(" ")
				}
			}
		} else if i == st.objects[st.curobj].line && i == st.ymax-1 {
			for j := 0; j < st.xmax; j++ {
				if st.onAgent(j) && st.onObj(j) {
					buf.WriteString("-")
				} else if st.onAgent(j) {
					buf.WriteString("#")
				} else if st.onObj(j) {
					buf.WriteString("*")
				} else {
					buf.WriteString(" ")
				}
			}
		} else {
			buf.WriteString("                                        ")
		}
		buf.WriteString("|\n")
	}

	buf.WriteString("+----------------------------------------+\n")

	return buf.String()
}

func (st *state) onAgent(pos int) bool {
	return onGeneric(pos, st.agent.offset, st.agent.size, st.xmax)
}

func (st *state) onObj(pos int) bool {
	return onGeneric(pos, st.objects[st.curobj].offset, st.objects[st.curobj].size, st.xmax)
}

func onGeneric(pos, offset, size, xmax int) bool {
	if pos < utils.Max(0, offset+size-xmax) ||
		(pos >= offset && pos < utils.Min(xmax, offset+size)) {
		return true
	}

	return false
}

type paddle struct {
	size int
	offset int
}

type object struct {
	size int
	offset int
	line int
	direction int
}

func generateObject() object {
	return object{size: rand.Intn(6)+1,
		offset: rand.Intn(40),
		line: 0,
		direction: rand.Intn(3)-1}
}

func generateGenotypes(popsize int) []ea.Genotype {
	ret := make([]ea.Genotype, popsize)

	for i := range ret {
		individual := make(mincogGenotype, ngenes)
		for j := range individual {
			individual[j] = uint8(rand.Intn(2))
		}
		ret[i] = individual
	}

	return ret
}

type mincogGenotype []uint8

func (s mincogGenotype) GeneratePhenotype() ea.Phenotype {
	out := make(mincogPhenotype, 4) // Weights, bias, gain, timec

	/* Weights */
	out[0] = make([]float64, 22)
	var i int
	for j := 0; j < len(out[0]); j++ { //TODO: Don't hardcode?
		out[0][j] = (decodeVector(s[i:i+8]) * 10.0) - 5.0 // [-5, 5]
		i+=8
	}

	/* Bias */
	out[1] = make([]float64, 9)
	for j := 0; j < len(out[1]); j++ {
		out[1][j] = decodeVector(s[i:i+8]) * -10.0 //20.0 - 10.0 // [-10, 0]
		i+=8
	}

	/* Gain */
	out[2] = make([]float64, 9)
	for j := 0; j < len(out[2]); j++ {
		out[2][j] = (decodeVector(s[i:i+8]) * 4.0) + 1.0 // [1, 5]
		i+=8
	}

	/* Timec */
	out[3] = make([]float64, 9)
	for j := 0; j < len(out[3]); j++ {
		out[3][j] = decodeVector(s[i:i+8]) + 1.0 // [1, 2]
		i+=8
	}

	return out
}

func decodeVector(bvector []uint8) float64 {
	var result uint8
	for i, x := range bvector {
		result += (x << uint8(7-i))
	}

	return float64(result)/255.0
}

func (s mincogGenotype) CrossoverWith(mate ea.Genotype) (ea.Genotype, ea.Genotype) {
	pivot := rand.Intn(ngenes)
	kid1 := make(mincogGenotype, ngenes)
	kid2 := make(mincogGenotype, ngenes)

	i := 0
	for ; i < pivot; i++ {
		kid1[i] = s[i]
		kid2[i] = uint8(mate.GetIthGene(i))
	}

	for ; i < ngenes; i++ {
		kid1[i] = uint8(mate.GetIthGene(i))
		kid2[i] = s[i]
	}

	return kid1, kid2
}

func (s mincogGenotype) Mutate(rate float64) {
	for i, bit := range s {
		if rate > rand.Float64() {
			if bit == 1 {
				s[i] = 0
			} else {
				s[i] = 1
			}
		}
	}
}

func (s mincogGenotype) GetIthGene(i int) float64 {
	return float64(s[i])
}

func (s mincogGenotype) Copy() ea.Genotype {
	out := make(mincogGenotype, len(s))
	copy(out, s)

	return out
}

type mincogPhenotype [][]float64

func (s mincogPhenotype) updateNetwork() {
//	network.Reset()
	network = ctrnn.NewCTRNN(5, 2, []int{2})
	network.SetWeights(s[0])
	network.SetVariables(s[1], s[2], s[3])
}

func (s mincogPhenotype) Fitness() float64 {
	s.updateNetwork()

	st := curstate.deepCopy()

	//capture, collision, _ := st.simulate(false)
	capture, collision, avoidance := st.simulate(false)

	return float64(capture) - 0.6 * float64(collision) - 0.1 * float64(avoidance)
	//return 2.0 * float64(capture) - 0.2 * float64(collision) - 10.0 * float64(avoidance)
	//return float64(capture)
	//return 1.0/(1.0+math.Exp(-float64(capture))+float64(avoidance))
	//return 2.0 * float64(capture) + 1.2 * float64(collision) - 3.0 * float64(avoidance)
	//return float64(totSumOn)-0.4*float64(totSumOff)
}

func interpretOutput(vals []float64) int {
	tot := (-vals[0] + vals[1]) * 4.5

	if tot > 4.0 {
		tot = 4.0
	} else if tot < -4.0 {
		tot = -4.0
	}

	/*
	tot := 4.0
	if vals[0] > vals[1] {
		tot *= -vals[0]
	} else {
		tot *= vals[1]
	}
	*/

	//fmt.Println("KUK", tot)
	return int(math.Ceil(tot))
}
