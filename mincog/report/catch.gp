set terminal png
set terminal epslatex
set output 'catch.tex'
set style fill solid 0.25 border -1
set style boxplot outliers pointtype 7
set style data boxplot
set boxwidth 0.5
set pointsize 0.5
set xtics nomirror
unset xtics
set ytics nomirror

set ylabel "Fitness"

unset key
set border 2

plot 'catch.dat' using (1.0):1
