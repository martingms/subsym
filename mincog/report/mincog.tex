\documentclass{article}
\usepackage{enumerate}
\usepackage{alltt}
\usepackage{amsmath}
\usepackage{mathtools}
\usepackage[margin=10pt]{subcaption}
\usepackage{hyperref}
\usepackage{tikz}
\usepackage{tabularx}
\usepackage{graphicx}
\usepackage[margin=0.9in]{geometry}
\usepackage[utf8]{inputenc}
%\usepackage[T1S]{fontenc}

\setlength{\parindent}{0.0in}
\setlength{\parskip}{0.1in}
\DeclareUnicodeCharacter{00A0}{ }

\begin{document}
\begin{titlepage}
   \vspace*{\fill}
    \begin{center}
	\textsc{\LARGE IT3708 Subsymbolic Methods in AI}\\[1.5cm]
	\textsc{\Large Project 3}\\[0.5cm]
	{ \huge \bfseries Evolving Neural Networks for a Minimally-Cognitive Agent\\[0.4cm] }

	Martin \textsc{Gammelsæter} -- \texttt{martigam@stud.ntnu.no}
	
    \vspace*{\fill}
	{\large \today}
	\end{center}
\thispagestyle{empty}
\end{titlepage}
\setcounter{page}{1}

\section{Introduction}
This report describes the use of an evolutionary algorithm to evolve the weights
and other parameters of a \emph{Continuous Time Recurrent Neural Network}, or
\emph{CTRNN}. This CTRNN is used to control an agent in the Tracker Environment
as specified in the project
description\footnote{See
\url{http://www.idi.ntnu.no/emner/it3708/assignments/modules/beer-min-cog.pdf}.}.
As the implementation of the EA-system has been thoroughly discussed in earlier
reports, this report will focus on the results of the system and the CTRNN
implementation.

\section{Implementation}
The system in its entirety is written in the Go programming language\footnote{See
\url{http://golang.org}.}. It consists of three main parts, the EA-system as
used in most of the projects, the CTRNN implementation, and a simulator for the
task at hand, the Tracker enviro8ment. An overview of the file architecture can
be seen in Fig.~\ref{fig:arch}. Generally, the \texttt{mincog.go} file
implements the game, its simulator and domain specific things like the fitness
function etcetera. From here, calls to \texttt{ea.go} for the evolutionary
parts, and \texttt{ctrnn.go} for the neural network.

\begin{figure}[h]
	\centering
		\includegraphics[scale=0.8]{arch.pdf}
	\caption{Overall architecture of the system}
	\label{fig:arch}
\end{figure}

The Tracker environment was run with a height of 15 blocks, a width of 40 and a
maximum agent speed of 4. The agent's size was 5 blocks, while the falling
objects' size was uniformly distributed between 1 to 6 blocks. These objects
fell at one block per tick, either directly down or one block to the right or
left each tick.

The EA-system evolves weights and other parameters, such as biases, time
constants and gains for the neural network. Genotypes are represented as simple
bit vectors, to facilitate simple mutation. To convert to phenotypes, groups of
eight bits are interpreted as 8-bit unsigned integers, which are then normalized
to a floating point number from 0.0 to 1.0 (see Fig.~\ref{fig:geno}).

\begin{figure}[h]
	\centering
	\texttt{[ ... 1 0 0 1 0 1 0 0 ... ]} $\rightarrow$ \texttt{148}
$\rightarrow$ \texttt{0.5803921568627451}
	\caption{The genotype to phenotype translation process}
	\label{fig:geno}
\end{figure}

Further, these floats are then scaled depending on if they represent a weight,
bias, time constant or gain value. These values are then used in the network for
fitness testing that phenotype. The EA parameters used when evolving the CTRNN
values can be seen in Table~\ref{table:eaparams}. As I had trouble finding a
fitness function (more on that later) that scaled well enough for tournament
selection to work as intended, rank selection was a nice substitute. It is
interesting to note the much less agressive parameters used in this project
compared to the first couple of projects with evolutionary algorithms, in which
a hyper-aggressive approach, using A-III adult selection and aggressive
tournament selection parameters, worked best. It goes to show that choosing EA
parameters is a very hard and domain dependent task.

\begin{table}[h]
  \centering
  \begin{tabular}{l | l}
    \textsc{Parameter} & \textsc{Value} \\
    \hline
	Pool size & 100 \\
	Generations & 500 \\
	Adult selection mechanism & A-I \\
	Parent selection mechanism & Rank selection \\
	Min & 0.7 \\
	Max & 1.4 \\
	Cross-over rate & 0.99 \\
	Per-gene mutation rate & 0.0001 \\	
	Elitism & 1 individual\\
  \end{tabular}
  \caption{EA-system parameters used in evolving CTRNN values} 
  \label{table:eaparams}
\end{table}

\section{Results}
\subsection{Catch-all problem}
To begin with, the task was ``simply'' to train the agent to properly catch
\emph{all} objects, even the large ones. With the fitness function used (see
Eq.~\ref{eq:fitnesscatchall}), any fitness of above $\sim30$ means that that
agent don't avoid any objects. With fitnesses above $\sim60$, the only objects
registered as collisions are the long ones that are impossible to properly
capture. Fig.~\ref{fig:catchall} is a boxplot of 23 evolutionary runs' best
fitness, showing that other than a few outliers (there was a total of two
avoided objects in these 23 runs), which happen from time to time, the system
successfully evolves agents that solves the catch-all problem.

\begin{equation}
	Fitness = 2.0 * Captured - 0.2 * Collided - 10.0 * Avoided
	\label{eq:fitnesscatchall}
\end{equation}

\begin{figure}
	\centering
		\input{catch}
	\caption{A boxplot (outliers as dots) showing best fitness of 23
evolutionary runs solving the catch-all problem}
	\label{fig:catchall}
\end{figure}

While tuning the system to achieve these results, it became clear that the
particularities of the fitness function was extremely important. In earlier
projects it has often been enough to just find a function that in some way
positively correlates with the goal behaviour, and the EA would converge towards
a near-optimal result. In this case however, it proved much harder. Deviating
only slightly from the fitness function in Eq.~\ref{eq:fitnesscatchall} often
proved disastrous for the results. While there obviously are many, many fitness
functions that would work for this task, this one was seemingly the one that fit
my system the best in this case.

While this fitness function lead to good results, it produces values that do not
scale well for use in e.g. tournament selection, or any other fitness
proportionate selection scheme. Therefore, using rank selection proved a perfect
match, mitigating the effects of the ``jumping'' fitness values.

\subsection{Full tracker problem}
No perfect solutions to the full tracker problem was found, but occasionally
fairly good solutions popped up. After much twiddling, it turned out that the
best results was found with a deceptively simple fitness function, as seen in
Eq.~\ref{eq:fitnessfulltracker}. Many others were tried, and a long time was
spent experimenting with much more complex functions similar to the catch-all
function in Eq.~\ref{eq:fitnesscatchall}. The function that performed best does
however not even consider the $Avoided$ statistic. It often errs on the side of
being to cautious though; rarely colliding but playing it safe and avoiding
more than it needs to. 

\begin{equation}
	Fitness = Captured - 0.6 * Collided
	\label{eq:fitnessfulltracker}
\end{equation}

Subjectively, a fitness-score of above $\sim22$ often shows signs of both
catching and avoiding, and as can be seen in Fig.~\ref{fig:fulltrackerboxplot},
such scores does surface occasionally. A fitness plot of one of the more
successful runs can be seen in Fig.~\ref{fig:fulltrackerfit}. The behaviour of
the agent in this particular run was generally moving fast to the right until
the sensors sensed something, then start tracking that object. When it
successfully avoided large objects the agent vibrated a bit, seemingly figured
out it was too large, and then backed off. This took some time however, and it
seemingly only worked on those large objects which the agent was able to track
very early in the object's trajectory.

\begin{figure}
	\centering
		\input{fullbox}
	\caption{A boxplot (outliers as dots) showing best fitness of 27
evolutionary runs solving the full tracker problem}
	\label{fig:fulltrackerboxplot}
\end{figure}

\begin{figure}
	\centering
		\input{fulltrackerfit}
	\caption{Fitness plot of a fairly successful run of the full tracker
problem. This particular specimen successfully captured 30 objects, avoided 4
objects, of which 3 were large, and collided with 6 objects}
	\label{fig:fulltrackerfit}
\end{figure}

\section{Modifications}
\subsection{Modification to scenario}
As a modification to the scenario, experiments with increasing the max speed of
the agent was performed. Before the modification the max speed in any direction
was 4 units per time step. Increasing it slightly, to 5 or 6 did not look like
it had any measurable effect, atleast not statistically speaking. Further
increasing the maximum to 16 had the interesting effect of deteriorating the
performance of the agent. Looking at the display, it looked like it brought big
issues of overshooting, which in hindsight is logical, even though one might
think that the network should be able to compensate for it.

\subsection{Modification to CTRNN topology}
CTRNN topology was modified by adding another two-neuron hidden layer to the
network. While it is hard to draw good conclusions with the relatively few
experiments run, this change seemed to make early convergence a much bigger
problem. With this extra layer the average best fitness went from $14.39$
(calculated over the same 27 runs as Fig.~\ref{fig:fulltrackerboxplot}) to
$9.1$ (calculated over 10 runs with the extra layer). Whether this is
statistically significant or not, no evidence was found that adding another
layer helped the agent in any way. In a way this is intuitive in that adding
more weights and more complexity increases the solution space for the EA to
search in greatly, and we have no reason to think that this extra space contains
more high-fitness solutions, so the increased space is a complete negative.

\subsection{Modification to CTRNN variables}
The bias variables of the CTRN-network was chosen for modification, changing the
allowed interval from $[-10, 0]$ to $[0, 10]$. After running ten experimental
runs with this new interval, no real change was found in the performance of the
agent. Upon inspection of the structure of the network however, it was found
that the system compensates for this change by changing predominantly positive
weights into a given neuron (that were weighted down by the negative bias) into
predominantly negative weights to instead weight down the now positive bias.
Whether or not changes to performance would be observed if experimented with on
a larger scale, the remarkable malleability of evolutionary trained neural
networks remains impressive.

\section{CTRNN analysis}
Studying a CTRN-network is considerably harder than a normal strict feedforward
network, since the calculations and connections involved are that much more
complex. Nonetheless, the network of a fairly successful agent was extracted and
analyzed in detail to see if any useful patterns could be identified. For
reference, the general topology of the network is the one depicted in Fig. 3 of
the project description\footnote{See
\url{http://www.idi.ntnu.no/emner/it3708/assignments/modules/beer-min-cog.pdf}.}

First sample networks response to a few different stimuli was tested, the results of
which can be seen in Table~\ref{table:inoutctrnn}.

\begin{table}[h]
  \centering
  \begin{tabular}{l | l | l}
    \textsc{Inputs} & \textsc{Outputs} & \textsc{Motor response} \\
    \hline
	\texttt{[0 0 0 0 0]} & \texttt{[0.00017000393427129052 0.0016011198569207143]} & $+1$ \\
	\texttt{[0 0 0 0 0]} & \texttt{[0.00021596449908055045 0.2982840223293501]} & $+2$ \\
	\texttt{[0 0 0 0 0]} & \texttt{[0.0002160465930814986 0.4555795965224191]} & $+3$ \\
	\texttt{[0 0 0 0 1]} & \texttt{[0.00022093177344859345 0.4640895343781981]} & $+3$ \\
	\texttt{[1 1 1 1 0]} & \texttt{[6.964399222816315e-06 4.18202617420964e-06]}
& $0$ \\
	\texttt{[1 1 1 1 0]} & \texttt{[0.09159817231838100223 8.1739846273939-07]}
& $-1$ \\
	\texttt{[0 0 0 1 1]} & \texttt{[0.00023018649298920876 0.4742799417418166]} & $+3$ \\
	\texttt{[1 1 1 1 1]} & \texttt{[0.143237129332539 1.9229667603716083e-08]} &
$-1$ \\
  \end{tabular}
  \caption{Input-ouput pairs in the CTRN-network} 
  \label{table:inoutctrnn}
\end{table}

When analyzing these results one should keep in mind that outputs also are
greatly affected by earlier outputs (``memory''), but these results shows us
a few things.

When all inputs are zero, that is, no shadows detected, it starts out going in a
speed of 1. When this same input vector repeats, the activation levels within
the output neurons increase, increasing the relative response.  In a functional
view, this is smart: If the agent doesn't sense something, it should pick up
speed and start looking around for an object.

Providing as input a single shadow in ``front'' of the agent makes it keep
going, but once it sees that it overshoots, it tries to back up to catch up
with the object again.

The last to examples in the table can also be rather simply explained. The first
one should obviously go right to explore the size of the object it detects. The
last one moves slowly around, as if to explore whether or not this is too big
and has to be dodged.

Admittedly, one could probably also find sequences of input-output pairs that
would make no sense if one tried to analyze them like this, but this at least
shows that some of the behaviour can be explained through analysis.

Going a little deeper, we can try to see \emph{how} the network achieves these
behaviours. The simplest behaviour to look into is why the agent always prefers
to go right (positive motor response) when it does not sense anything.
Seemingly, we do not have to dig deeper than to look at the bias values on the
two output neurons. The neuron that dictates right-ward motion has a bias of
practically zero (-0.00392), while the other output neuron (dictating left-ward
motion) has a bias term of -5.9019, slightly above the average of the network as
a whole. Additionally, the right-ward neuron has a gain of 4.2156, versus the
left-ward neuron's 1.72. Finally, the right-ward neuron's self-weight is a
positive 1.27, while the left-ward neuron has a negative -4.6, greatly
discouraging repeated left-ward motion.

Looking at the weights deeper into the network, we see that the two hidden-layer
neurons ``split'' the input neurons between them in a curious way. Neuron 5 has
negative or near-zero weights from every input neuron but neuron 0 and 4. Neuron 1,
2 and 3 are predominantly activating Neuron 6. Neuron 6's weights
to the two output neurons, 7 and 8, are both negative (-4.33 and -2.09
respectively), inhibiting motion in any direction. Presumably this could explain
the staying-in-place behaviour that is observed when the middle sensors are
active. Neuron 5 on the other hand, predominantly activated by the edge
sensors, has strongly positive weights to both output neurons, promoting motion
when the edges are active.

\end{document}
