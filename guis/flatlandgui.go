package main

import (
	"bufio"
//	"fmt"
	"os"

	"../lib/termbox-go"
)

var (
	step int
	scenarios [][]string
)

func main() {
	file, _ := os.Open(os.Args[1])

	scenarios = [][]string{}

	scanner := bufio.NewScanner(file)
	for scanner.Scan() {
		str := scanner.Text()
		if str[0] == '+' {
			scene := make([]string, 8)
			for i := 0; i < 8; i++ {
				scanner.Scan()
				scene[i] = scanner.Text()
			}
			scanner.Scan()
			scanner.Text() // Drop +

			scenarios = append(scenarios, scene)
		}
	}

	if err := scanner.Err(); err != nil {
		panic(err)
	}

	termbox.Init()
	defer termbox.Close()
	termbox.SetInputMode(termbox.InputEsc)
	termbox.Clear(termbox.ColorDefault, termbox.ColorDefault)
	draw()

gui_loop:
	for {
	event_switch:
		switch ev := termbox.PollEvent(); ev.Type {
		case termbox.EventKey:
			if ev.Key == termbox.KeyCtrlC {
				break gui_loop
			}
			if ev.Key == termbox.KeyEnter {
				if step < len(scenarios)-1 {
					step++
				} else {
					break gui_loop
				}
				draw()
				break event_switch
			}
		case termbox.EventResize:
			draw()
			break event_switch
		case termbox.EventError:
			panic(ev.Err)
		}
	}
}

func draw() {
	//width, height := termbox.Size()
	termbox.Clear(termbox.ColorDefault, termbox.ColorDefault)

	for i, line := range scenarios[step] {
		for j, r := range line[1:len(line)-1] {
			drawRune(j, i, r)
		}
	}
	termbox.Flush()
}

func drawRune(i, j int, r rune) {
	if r == 'F' {
		termbox.SetCell(i, j, r, termbox.ColorDefault, termbox.ColorGreen)
	} else if r == 'P' {
		termbox.SetCell(i, j, r, termbox.ColorDefault, termbox.ColorRed)
	} else if r != ' ' {
		termbox.SetCell(i, j, r, termbox.ColorDefault, termbox.ColorBlue)
	} else {
		//termbox.SetCell(i, j, ' ', termbox.ColorDefault, termbox.ColorDefault)
	}
}
