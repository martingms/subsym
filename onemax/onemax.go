package main

import (
	"../lib/ea"
	"../lib/utils"
	"flag"
	"fmt"
	"math/rand"
	"runtime"
	"time"
)

var (
	v            = flag.Bool("v", true, "show verbose output")
	plotfile     = flag.String("plotfile", "", "filename to plot to. Leave empty to not plot")
	randtarget   = flag.Bool("randtarget", false, "let target be a random bitvector")
	targetvector []uint8

	bitnumber = flag.Int("b", 40, "number of bits in the genotypes")

	mju   = flag.Int("mju", 350, "adult pool size")
	delta = flag.Int("delta", 350, "children pool size")

	K       = flag.Int("K", 10, "tournament selection group size")
	epsilon = flag.Float64("epsilon", 0.05, "1 - epsilon chance of best fit chosen")

	min = flag.Float64("min", 0.5, "min value in rank selection")
	max = flag.Float64("max", 1.5, "max value in rank selection")

	gens            = flag.Int("gens", 100, "max generations")
	xoverRate       = flag.Float64("xr", 0.99, "crossover rate")
	mutationRate    = flag.Float64("mr", 0.000005, "mutation rate")
	adultSelection  = flag.String("aselection", "A-I", "adult selection method (A-I, A-II, A-III)")
	parentSelection = flag.String("pselection", "fp", "parent selection method (fp, ss, ts, rs)")
)

func init() {
	runtime.GOMAXPROCS(runtime.NumCPU())
	rand.Seed(time.Now().UTC().UnixNano())
	flag.Parse()
}

func main() {
	plot := false
	if *plotfile != "" {
		plot = true
		utils.InitFileWrite(*plotfile + ".dat")
	}

	if *randtarget {
		targetvector = make([]uint8, *bitnumber)
		for i := range targetvector {
			targetvector[i] = uint8(rand.Intn(2))
		}
	}

	utils.Logln("Generating random genotypes...")
	population := generateGenotypes(*delta)
	utils.Logln("Initializing ea...")
	earun, err := ea.NewEa(*mju, *delta, *K, population, *epsilon, *xoverRate,
		*mutationRate, *min, *max, *adultSelection, *parentSelection, false)
	if err != nil {
		panic(err)
	}

	maxFitness := float64(*bitnumber)

	var i int
	for i = 0; i < *gens && earun.BestFitness < maxFitness; i++ {
		utils.Logln("Iteration", i, "...")
		err := earun.RunIteration()
		if err != nil {
			panic(err)
		}
		utils.Logln("Best fitness:", earun.BestFitness)
		utils.Logln("Best individual:", earun.BestGenotype)
		if plot {
			max, avg, stddev := earun.RunStats()
			utils.Write(fmt.Sprintf("%v %v %v %v %v\n", i, max, avg, avg-stddev, avg+stddev))
		}
	}
	if plot {
		utils.FinalizeWrite()
		utils.Plot(*plotfile+".dat", *plotfile+".tex", i-1)
	}
}

func generateGenotypes(popsize int) []ea.Genotype {
	ret := make([]ea.Genotype, popsize)

	for i := range ret {
		individual := make(oneMaxGenotype, *bitnumber)
		for j := range individual {
			individual[j] = uint8(rand.Intn(2))
		}
		ret[i] = individual
	}

	return ret
}

// In other words, does not allow number of bits to be higher than 64.
type oneMaxGenotype []uint8

func (s oneMaxGenotype) GeneratePhenotype() ea.Phenotype {
	return oneMaxPhenotype(s)
}

func (s oneMaxGenotype) CrossoverWith(mate ea.Genotype) (ea.Genotype, ea.Genotype) {
	pivot := rand.Intn(*bitnumber)
	kid1 := make(oneMaxGenotype, *bitnumber)
	kid2 := make(oneMaxGenotype, *bitnumber)

	i := 0
	for ; i < pivot; i++ {
		kid1[i] = s[i]
		kid2[i] = uint8(mate.GetIthGene(i))
	}

	for ; i < *bitnumber; i++ {
		kid1[i] = uint8(mate.GetIthGene(i))
		kid2[i] = s[i]
	}

	return kid1, kid2
}

func (s oneMaxGenotype) Mutate(rate float64) {
	for i, bit := range s {
		if rate > rand.Float64() {
			if bit == 1 {
				s[i] = 0
			} else {
				s[i] = 1
			}
		}
	}
}

func (s oneMaxGenotype) GetIthGene(i int) float64 {
	return float64(s[i])
}

func (s oneMaxGenotype) Copy() ea.Genotype {
	out := make(oneMaxGenotype, len(s))
	copy(out, s)

	return out
}

type oneMaxPhenotype []uint8

func (s oneMaxPhenotype) Fitness() float64 {
	if *randtarget {
		var sum uint8
		for i, bit := range s {
			if bit == targetvector[i] {
				sum += 1
			}
		}

		return float64(sum)

	}

	var sum uint8
	for _, bit := range s {
		sum += bit
	}

	return float64(sum)
}
