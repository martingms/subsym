package main

import (
	"runtime"
	"bytes"
	"io/ioutil"
	"flag"
	"fmt"
	"os"
	"strconv"
	"strings"
	"../lib/utils"
	"math"
	"math/rand"
	"time"
)

var (
	m = flag.Int("M", 34, "M-variable")

	g graph
	fin graph
	nnodes int
	d int
	p float64
	bslice []rune
)

func init() {
	runtime.GOMAXPROCS(runtime.NumCPU())
	rand.Seed(time.Now().UTC().UnixNano())
	flag.Parse()
}

func main() {
	if flag.NArg() != 1 {
		fmt.Fprintf(os.Stderr, "Please supply a graph file.")
		os.Exit(1)
	}

	g, nnodes, d = parseFile(flag.Arg(0))

	p = 1.0/float64(d)
	fin = graph{}
	maxiter := int(float64(*m) * math.Log2(float64(nnodes))) + 1
	bslice = make([]rune, nnodes)

	for {
		if p >= 1.0 {
			break
		}


		for i := 0; i < maxiter; i++ {
			for i := range bslice {
				bslice[i] = 0x0
			}
			for _, n := range g {
				n.sendB()
			}
			for _, n := range g {
				n.checkNeighborsRoundOne()
			}
			for _, n := range g {
				if n.state == 0 {
					n.checkNeighborsRoundTwo()
				}
			}
		}

		p *= 2.0
	}

	fmt.Println(fin)

	fmt.Fprintln(os.Stderr, "Violated constraints:", fin.calculateViolatedConstraints(g))

}

type node struct {
	idx int
	state uint8
	x float64
	y float64
	color string

	neighbors []int
}

func (n *node) sendB() {
	if rand.Float64() < p {
		bslice[n.idx] = 'B'
		n.state = 1
	}
}

func (n *node) checkNeighborsRoundOne() {
	for _, idx := range n.neighbors {
		if bslice[idx] == 'B' {
			n.state = 0
			break
		}
	}

	if n.state == 1 {
		n.color = "L"
		fin[n.idx] = n
		delete(g, n.idx)
	} else {
		bslice[n.idx] = 0x0
	}
}

func (n *node) checkNeighborsRoundTwo() {
	for _, idx := range n.neighbors {
		if bslice[idx] == 'B' {
			n.color = "NL"
			fin[n.idx] = n
			delete(g, n.idx)
			break
		}
	}
}

func (n *node) gvColor() string {
	switch n.color {
	case "NL":
		return "blue"
	case "L":
		return "red"
	default:
		panic("unknown color")
	}
}

type graph map[int]*node

func (g graph) calculateViolatedConstraints(start graph) int {
	broken := len(start) // Nodes not in L or NL
	for _, node := range g {
		var foundLeader bool = false
		if node.color == "L" {
			foundLeader = true
		}

		for _, neighborIdx := range node.neighbors {
			if node.color == "L" && g[neighborIdx].color == "L" {
				broken++ // Two adjacent nodes both in L
			} else if !foundLeader && g[neighborIdx].color == "L" {
				foundLeader = true
			}
		}

		if !foundLeader {
			broken++ // No neighbors or self in L
		}
	}

	return broken
}

func parseFile(filename string) (graph, int, int) {
	content, err := ioutil.ReadFile(filename)
	utils.PanicIfErr(err)

	str := strings.Replace(string(content), "\r", "", -1)

	lines := strings.Split(str, "\n")

	linecounter := 0

	lineone := strings.Split(lines[linecounter], " ")
	linecounter++

	nnodes, err := strconv.Atoi(lineone[0])
	utils.PanicIfErr(err)
	nedges, err := strconv.Atoi(lineone[1])
	utils.PanicIfErr(err)

    g := graph{}
    for ; linecounter < nnodes + 1; linecounter++ {
        line := strings.Split(lines[linecounter], " ")
        idx, err := strconv.Atoi(line[0])
		utils.PanicIfErr(err)
        x, err := strconv.ParseFloat(line[1], 64)
		utils.PanicIfErr(err)
        y, err := strconv.ParseFloat(line[2], 64)
		utils.PanicIfErr(err)
        g[idx] = &node{idx: idx, x: x, y: y, color: "NONE", neighbors: []int{}}
    }

	dcounter := make([]int, nnodes)
    for ; linecounter < nnodes + nedges + 1; linecounter++ {
        line := strings.Split(lines[linecounter], " ")
        from, err := strconv.Atoi(line[0])
		utils.PanicIfErr(err)
        to, err := strconv.Atoi(line[1])
		utils.PanicIfErr(err)

		g[from].neighbors = append(g[from].neighbors, to)
		g[to].neighbors = append(g[to].neighbors, from)

		dcounter[from]++
		dcounter[to]++
    }

	return g, nnodes, utils.MaxIntSlice(dcounter)
}

func (g graph) String() string {
    var str bytes.Buffer
    str.WriteString("graph state {\n")
	str.WriteString("       maxiter=0;\n")

    for i, node := range g {
        str.WriteString("       ")
        str.WriteString(strconv.Itoa(i))
        str.WriteString(" [fillcolor=")
        str.WriteString(node.gvColor())
        str.WriteString(", pos=\"")
        str.WriteString(strconv.FormatFloat(node.x, 'f', 2, 32))
        str.WriteString(",")
        str.WriteString(strconv.FormatFloat(node.y, 'f', 2, 32))
        str.WriteString("!\", shape=circle, style=filled];\n")
    }

    for from, node := range g {
        for _, to := range node.neighbors {
            if from < to {
                str.WriteString("       ")
                str.WriteString(strconv.Itoa(from))
                str.WriteString(" -- ")
                str.WriteString(strconv.Itoa(to))
                str.WriteString(";\n")
            }
        }
    }
    str.WriteString("}\n")

    return str.String()
}

