package main

import (
	"runtime"
	"bytes"
	"io/ioutil"
	"flag"
	"fmt"
	"os"
	"strconv"
	"strings"
	"../lib/utils"
	"math"
	"math/rand"
	"time"
)

var (
	m = flag.Int("M", 34, "M-variable")
	k = flag.Int("K", 3, "number of colors")
	pforget = flag.Float64("p", 0.1, "chance to forget color")

	g graph
	fin graph
	nnodes int
	d int
	p float64
	cslice []uint8
)

func init() {
	runtime.GOMAXPROCS(runtime.NumCPU())
	rand.Seed(time.Now().UTC().UnixNano())
	flag.Parse()
}

func main() {
	if flag.NArg() != 1 {
		fmt.Fprintf(os.Stderr, "Please supply a graph file.")
		os.Exit(1)
	}

	g, nnodes, d = parseFile(flag.Arg(0))

	p = 1.0/float64(d)
	fin = graph{}
	maxiter := int(float64(*m) * math.Log2(float64(nnodes))) + 1
	cslice = make([]uint8, nnodes)

	for {
		if p >= 1.0 {
			break
		}


		for i := 0; i < maxiter; i++ {
			/*for i := range cslice {
				cslice[i] = 0
			}*/
			for _, n := range g {
				n.sendC()
			}
			for _, n := range g {
				n.checkNeighborsRoundOne()
			}
			for _, n := range g {
				if n.state == 0 {
					n.checkNeighborsRoundTwo()
				}
			}
		}

		p *= 2.0
	}

	fmt.Println(g)

	fmt.Fprintln(os.Stderr, "Violated constraints:", g.calculateViolatedConstraints(g))

}

type node struct {
	idx int
	state uint8
	x float64
	y float64
	color uint8

	availableColors map[uint8]struct{}
	neighbors []int
}

func (n *node) String() string {
	var buf bytes.Buffer
	buf.WriteString("idx: " + strconv.Itoa(n.idx))
	buf.WriteString("\nstate: " + strconv.Itoa(int(n.state)))
	buf.WriteString("\ncolor: " + strconv.Itoa(int(n.color)))
	buf.WriteString("\nAvailableColors:\n	")
	for c := range n.availableColors {
		buf.WriteString(strconv.Itoa(int(c)))
		buf.WriteString(" ")
	}
	buf.WriteString("\nNeighbors:\n   ")
	for _, ne := range n.neighbors {
		buf.WriteString(strconv.Itoa(ne))
		buf.WriteString(" ")
	}

	return buf.String()
}

func (n *node) sendC() {
	if n.state == 0 {
		if rand.Float64() < p {
			c := n.pickColor()
			if c != 0 {
				cslice[n.idx] = c
				n.state = 1
			}
		}
	} else if rand.Float64() < *pforget {
		for _, idx := range n.neighbors {
			if len(g[idx].availableColors) == 0 && g[idx].color == 0 {
				for _, inneri := range n.neighbors {
					g[inneri].availableColors[n.color] = struct{}{}
				}
				cslice[n.idx] = 0
				n.color = 0
				n.state = 0
				break
			}
		}
	}
}

func (n *node) checkNeighborsRoundOne() {
	for _, idx := range n.neighbors {
		if cslice[idx] == cslice[n.idx] {
			n.state = 0
			break
		}
	}

	if n.state == 1 {
		n.color = cslice[n.idx]
	} else {
		cslice[n.idx] = 0
		n.color = 0
	}
}

func (n *node) checkNeighborsRoundTwo() {
	for _, idx := range n.neighbors {
		if cslice[idx] == cslice[n.idx] && cslice[idx] != 0 {
			panic("cslice[idx] == cslice[n.idx]")
		}
		delete(n.availableColors, cslice[idx])
	}
}

func (n *node) gvColor() string {
	switch n.color {
	case 1:
		return "blue"
	case 2:
		return "red"
	case 3:
		return "green"
	case 4:
		return "olivedrab"
	case 5:
		return "orangered"
	case 6:
		return "yellow"
	case 7:
		return "saddlebrown"
	case 8:
		return "white"
	case 9:
		return "deepskyblue"
	case 10:
		return "springgreen"
	default:
		return "black"
	}

	panic("unreachable")
}

func (n *node) pickColor() uint8 {
	if len(n.availableColors) == 0 {
		return 0
	}

	roulette := rand.Intn(len(n.availableColors))

	for c := range n.availableColors {
		if roulette == 0 {
			return c
		}

		roulette--
	}

	panic("unreachable")
}

type graph map[int]*node

func (g graph) calculateViolatedConstraints(start graph) int {
	broken := 0
	for _, node := range g {
		for _, neighborIdx := range node.neighbors {
			if node.color == g[neighborIdx].color && node.color != 0 {
				broken++
			}
		}
	}

	broken /= 2

	for _, node := range g {
		if node.color == 0 {
			broken++
		}
	}

	return broken
}

func parseFile(filename string) (graph, int, int) {
	content, err := ioutil.ReadFile(filename)
	utils.PanicIfErr(err)

	str := strings.Replace(string(content), "\r", "", -1)

	lines := strings.Split(str, "\n")

	linecounter := 0

	lineone := strings.Split(lines[linecounter], " ")
	linecounter++

	nnodes, err := strconv.Atoi(lineone[0])
	utils.PanicIfErr(err)
	nedges, err := strconv.Atoi(lineone[1])
	utils.PanicIfErr(err)

    g := graph{}
    for ; linecounter < nnodes + 1; linecounter++ {
        line := strings.Split(lines[linecounter], " ")
        idx, err := strconv.Atoi(line[0])
		utils.PanicIfErr(err)
        x, err := strconv.ParseFloat(line[1], 64)
		utils.PanicIfErr(err)
        y, err := strconv.ParseFloat(line[2], 64)
		utils.PanicIfErr(err)
        g[idx] = &node{idx: idx, x: x, y: y, color: 0, neighbors: []int{}}
		g[idx].availableColors = make(map[uint8]struct{})
		var i uint8 = 1
		for ; i <= uint8(*k); i++ {
			g[idx].availableColors[i] = struct{}{}
		}
    }

	dcounter := make([]int, nnodes)
    for ; linecounter < nnodes + nedges + 1; linecounter++ {
        line := strings.Split(lines[linecounter], " ")
        from, err := strconv.Atoi(line[0])
		utils.PanicIfErr(err)
        to, err := strconv.Atoi(line[1])
		utils.PanicIfErr(err)

		g[from].neighbors = append(g[from].neighbors, to)
		g[to].neighbors = append(g[to].neighbors, from)

		dcounter[from]++
		dcounter[to]++
    }

	return g, nnodes, utils.MaxIntSlice(dcounter)
}

func (g graph) String() string {
    var str bytes.Buffer
    str.WriteString("graph state {\n")
	str.WriteString("       maxiter=0;\n")

    for i, node := range g {
        str.WriteString("       ")
        str.WriteString(strconv.Itoa(i))
        str.WriteString(" [fillcolor=")
        str.WriteString(node.gvColor())
        str.WriteString(", pos=\"")
        str.WriteString(strconv.FormatFloat(node.x, 'f', 2, 32))
        str.WriteString(",")
        str.WriteString(strconv.FormatFloat(node.y, 'f', 2, 32))
        str.WriteString("!\", shape=circle, style=filled];\n")
    }

    for from, node := range g {
        for _, to := range node.neighbors {
            if from < to {
                str.WriteString("       ")
                str.WriteString(strconv.Itoa(from))
                str.WriteString(" -- ")
                str.WriteString(strconv.Itoa(to))
                str.WriteString(";\n")
            }
        }
    }
    str.WriteString("}\n")

    return str.String()
}

