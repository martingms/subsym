package main

import (
	"../lib/ea"
	"../lib/utils"
	"flag"
	"fmt"
	"math/rand"
	"runtime"
	"time"
)

var (
	v    = flag.Bool("v", true, "show verbose output")
	mode = flag.String("mode", "global", "global or local sequences")

	symbolsetSize  = flag.Int("S", 2, "number of symbols in the symbol set")
	sequenceLength = flag.Int("L", 2, "sequence length to be searched for")

	mju   = flag.Int("mju", 200, "adult pool size")
	delta = flag.Int("delta", 300, "children pool size")

	K       = flag.Int("K", 10, "tournament selection group size")
	epsilon = flag.Float64("epsilon", 0.1, "1 - epsilon chance of best fit chosen")

	min = flag.Float64("min", 0.5, "min value in rank selection")
	max = flag.Float64("max", 1.5, "max value in rank selection")

	gens            = flag.Int("gens", 10000, "max generations")
	xoverRate       = flag.Float64("xr", 0.85, "crossover rate")
	mutationRate    = flag.Float64("mr", 0.003, "mutation rate")
	adultSelection  = flag.String("aselection", "A-III", "adult selection method (A-I, A-II, A-III)")
	parentSelection = flag.String("pselection", "ts", "parent selection method (fp, ss, ts, rs)")
)

func init() {
	runtime.GOMAXPROCS(runtime.NumCPU())
	rand.Seed(time.Now().UTC().UnixNano())
	flag.Parse()
}

func main() {
	utils.Logln("Generating random genotypes...")
	population := generateGenotypes()
	utils.Logln("Initializing ea...")
	earun, err := ea.NewEa(*mju, *delta, *K, population, *epsilon, *xoverRate, *mutationRate, *min, *max, *adultSelection, *parentSelection, false)
	if err != nil {
		panic(err)
	}

	for i := 0; i < *gens && earun.BestFitness < 1.0; i++ {
		utils.Logln("Iteration", i, "...")
		err := earun.RunIteration()
		if err != nil {
			panic(err)
		}
		utils.Logln("Best fitness:", earun.BestFitness)
		utils.Logln("Best individual:", earun.BestGenotype)
	}
}

func generateGenotypes() []ea.Genotype {
	ret := make([]ea.Genotype, *delta)

	for i := range ret {
		individual := make(surprisingSeqGenotype, *sequenceLength)
		for j := range individual {
			individual[j] = uint8(rand.Intn(*symbolsetSize))
		}
		ret[i] = individual
	}

	return ret
}

type surprisingSeqGenotype []uint8

func (s surprisingSeqGenotype) GeneratePhenotype() ea.Phenotype {
	return surprisingSeqPhenotype(s)
}

// TODO: %s,sequenceLength,len(s),g and otherwise identical with onemax-version.. util!
func (s surprisingSeqGenotype) CrossoverWith(mate ea.Genotype) (ea.Genotype, ea.Genotype) {
	pivot := rand.Intn(*sequenceLength)
	kid1 := make(surprisingSeqGenotype, *sequenceLength)
	kid2 := make(surprisingSeqGenotype, *sequenceLength)

	i := 0
	for ; i < pivot; i++ {
		kid1[i] = s[i]
		kid2[i] = uint8(mate.GetIthGene(i))
	}

	for ; i < *sequenceLength; i++ {
		kid1[i] = uint8(mate.GetIthGene(i))
		kid2[i] = s[i]
	}

	return kid1, kid2
}

func (s surprisingSeqGenotype) Mutate(rate float64) {
	for i := range s {
		if rate > rand.Float64() {
			s[i] = uint8(rand.Intn(*symbolsetSize))
		}
	}
}

func (s surprisingSeqGenotype) GetIthGene(i int) float64 {
	return float64(s[i])
}

func (s surprisingSeqGenotype) Copy() ea.Genotype {
	out := make(surprisingSeqGenotype, len(s))
	copy(out, s)

	return out
}

type surprisingSeqPhenotype []uint8

func (s surprisingSeqPhenotype) Fitness() float64 {
	switch *mode {
	case "global":
		done := map[string]int{}
		faults := 0
		for i, gene := range s {
			for j := 0; j < len(s)-i-1; j++ {
				str := fmt.Sprintf("%v-%v-%v", gene, j, s[i+j+1])
				done[str]++
				if done[str] > 1 {
					faults++
				}
			}
		}

		return 1.0 / (float64(faults) + 1.0)
	case "local":
		done := map[string]int{}
		faults := 0
		for i := 0; i < len(s)-1; i++ {
			str := fmt.Sprintf("%v-%v", s[i], s[i+1])
			done[str]++
			if done[str] > 1 {
				faults++
			}
		}

		return 1.0 / (float64(faults) + 1.0)
	default:
		panic("Invalid mode")
	}
	panic("unreachable")
}
