\documentclass{article}
\usepackage{enumerate}
\usepackage{alltt}
\usepackage{amsmath}
\usepackage{mathtools}
\usepackage[margin=10pt]{subcaption}
\usepackage{hyperref}
\usepackage{tikz}
\usepackage{tabularx}
\usepackage{graphicx}
\usepackage[margin=.7in]{geometry}
\usepackage[utf8]{inputenc}
%\usepackage[T1S]{fontenc}

\setlength{\parindent}{0.0in}
\setlength{\parskip}{0.1in}
\DeclareUnicodeCharacter{00A0}{ }

\begin{document}
\begin{titlepage}
   \vspace*{\fill}
    \begin{center}
	\textsc{\LARGE IT3708 Subsymbolic Methods in AI}\\[1.5cm]
	\textsc{\Large Project 2}\\[0.5cm]
	{ \huge \bfseries Programming the Basics of an Evolutionary Algorithm and
Searching for Surprising Sequences\\[0.4cm] }

	Martin \textsc{Gammelsæter} -- \texttt{martigam@stud.ntnu.no}
	
    \vspace*{\fill}
	{\large \today}
	\end{center}
\thispagestyle{empty}
\end{titlepage}
\setcounter{page}{1}

\section{Introduction}
This report is split into two parts, one describing a framework for evolutionary
algorithms written by the author, along with a simple problem used to test it;
and a second describing the use of this framework in searching for surprising
sequences\footnote{See \url{http://aleph0.clarku.edu/~djoyce/mpst/surprising/}
for a thorough description of the problem}. This report will mainly focus on the
authors implementation and results, so explaining the theory behind evolutionary
algorithms is beyond the intended scope. For a good introduction to these
topics, refer to the literature.

\section{Implementation and interface}
The framework is written in the Go programming language\footnote{See
\url{http://golang.org}.}, utilizing interface constructs to make up for the
lack of generics in the language. Go was chosen as a good compromise between
ease and speed of development, and efficiency. The framework is nowhere near
perfectly optimized, trying to err on the side of readability, but some effort
was put into making the implementation as efficient, as well as user friendly,
as possible. The framework mimics Figure 1 in the provided
\texttt{ea-appendices.pdf}\footnote{Found here:
\url{http://www.idi.ntnu.no/emner/it3708/lectures/notes/ea-appendices.pdf}} as
close as possible, implementing all the required steps.

\subsection{Code structure}
A rough overview of the structure of the code can be seen in
Fig.~\ref{fig:codestructure}. The file \texttt{ea.go} contains the entire
framework (except for a few convenience functions contained in
\texttt{utils.go}), which the actual problem programs, \texttt{onemax.go} and
\texttt{surprisingseq.go}, calls into.

\begin{figure}[h]
	\centering
	\include{codestructure}
	\caption{The code structure of the project}
	\label{fig:codestructure}
\end{figure}

The API and workflow is fairly simple:
\begin{itemize}
	\item To initialize, call \texttt{NewEa()}, supplying it with all the
different parameters such as mutation rate and crossover rate, as well as the
initial population.
	\item For each iteration, call \texttt{RunIteration()}, which handles
everything EA-specific. The calling program is responsible for keeping track of
how many iterations to run before exiting.
	\item The calling program can at any time access the current best fitness
and best performing individual through the object returned by \texttt{NewEa()}
\end{itemize}

When implementing a specific problem to run with the framework, one has to
define a couple of types that has to adhere to a interface for the framework to
function. The initial population supplied to the \texttt{NewEa()} call has to
be a slice (array) of a datatype that implements the interface
\texttt{Genotype} as specified in Fig.~\ref{fig:genotypeinterface}. From the
genotype one can generate phenotypes. These must implement the
\texttt{Phenotype} interface that can be seen in
Fig.~\ref{fig:phenotypeinterface}.

\begin{figure}
\centering
\begin{subfigure}{.5\textwidth}
  \centering
	\begin{verbatim}
type Genotype interface {
    CrossoverWith(Genotype) (Genotype, Genotype)
    GetIthGene(int) uint8
    Mutate(float64)
    GeneratePhenotype() Phenotype
    Copy() Genotype
}
	\end{verbatim}
  \caption{The \texttt{Genotype} interface}
  \label{fig:genotypeinterface}
\end{subfigure}
\begin{subfigure}{.5\textwidth}
  \centering
	\begin{verbatim}
type Phenotype interface {
    Fitness() float64
}
	\end{verbatim}
  \caption{The \texttt{Phenotype} interface}
  \label{fig:phenotypeinterface}
\end{subfigure}
\caption{The two interfaces that must be implemented for the framework to function}
\label{fig:interfaces}
\end{figure}

To summarize, if one is using the framework for a new problem, one simply has
to:

\begin{itemize}
	\item Make genotype and phenotype types that implement the respective
interfaces, such as custom mutation, phenotype generation, crossover function,
and fitness function.
	\item Generate a random initial population, and initialize the framework
with all wanted parameters with the \texttt{NewEa()} call.
	\item Call \texttt{RunIteration()} for every iteration one wants to run.
\end{itemize}

While the Go programming language's lack of generics makes API-construction
tricky, this shows that using the framework for new problems is trivial. Adding
new adult or parent selection mechanisms is similarly easy.

\section{The One-Max problem}
In Fig.~\ref{fig:onemaxfpai} we can see fitness plots of different runs of the
EA framework utilized for the One-Max problem of 40 bits. It was found that a
generation size of $\sim350$ individuals was enough to consistently find a
solution to the 40-bit One-Max problem within 100 generations when using fitness
proportionate parent selection and full generational replacement (see
Fig.~\ref{fig:onemaxfpAI350.99.0005}). Keeping the generation size and selection
mechanisms consistent, the mutation rate and crossover rate was experimented
with. The results from this experimentation was fairly conclusive and intuitive.
When halving the crossover rate from 0.99 to 0.5, the EA still converges fairly
quickly, but noticably slower than when the crossover rate is practically 1, one
example of which can be seen in Fig.~\ref{fig:onemaxfpAI350.05.0005}.  It is
fairly intuitive that the heritability of simple crossover in the One-Max
problem is very good, since no combination of bits have any meaning. Hence there
is no reason to not crossover when mating parents, and we can conclude that for
this problem a high crossover rate is more effective than a low one.

\subsection{Optimizing mutation and crossover rates}
The mutation rate is however a bit more interesting. When using fitness
proportionate and A-I selection mechanisms, it was found that the mutation rate
had a very high impact on the solveability of the One-Max problem. When simply
doubling the per genome component mutation rate from 0.005 to 0.01, the amount
of time needed to converge more than doubled (see
Fig.~\ref{fig:onemaxfpAI350.099.001}). Going the other way and reducing the
mutation rate from 0.005 showed very slight speedups
(Fig.~\ref{fig:onemaxfpAI350.099.0000005})) all the way down to a mutation rate
of 0. Statistically however, it is impossible to conclude from the conducted
tests what is the best mutation rate as long as it is below $\sim0.005$.

\begin{figure}
\centering
\begin{subfigure}{.4\textwidth}
  \centering
  \scalebox{.6}{\input{onemax-fp-A_I-350i-099-0005}}
 
  \caption{OneMax problem run with fitness proportionate and A-I replacement,
generation size 350, crossover rate 0.99 and a per genome component mutation
rate of 0.005}
  \label{fig:onemaxfpAI350.99.0005}
\end{subfigure}
\begin{subfigure}{.4\textwidth}
  \centering
  \scalebox{.6}{\input{onemax-fp-A_I-350i-05-0005}}
  \caption{Same configuration as in Fig.~\ref{fig:onemaxfpAI350.99.0005}, but with
crossover rate turned down to 0.5} 
  \label{fig:onemaxfpAI350.05.0005}
\end{subfigure}
\begin{subfigure}{.4\textwidth}
  \centering
  \scalebox{.6}{\input{onemax-fp-A_I-350i-099-001}}
  \caption{Same configuration as in Fig.~\ref{fig:onemaxfpAI350.99.0005}, but with
mutation rate doubled to 0.01} 
  \label{fig:onemaxfpAI350.099.001}
\end{subfigure}
\begin{subfigure}{.4\textwidth}
  \centering
  \scalebox{.6}{\input{onemax-fp-A_I-350i-099-0000005}}
  \caption{Same configuration as in Fig.~\ref{fig:onemaxfpAI350.99.0005}, but with
mutation rate lowered to 0.000005} 
  \label{fig:onemaxfpAI350.099.0000005}
\end{subfigure}
  \caption{Different One-Max 40-bit runs, all with fitness proportionate selection and A-I
replacement mechanisms}
  \label{fig:onemaxfpai}
\end{figure}

\subsection{Optimizing parent selection}
After finding good values for the crossover and mutation rates, experiments with
the different parent selection mechanisms was performed.  The framework
currently implements the following mechanisms: Fitness proportionate, sigma
scaling, rank selection and tournament selection. Tests done with each of these
mechanisms, with all other parameters equal, can be seen in
Fig.~\ref{fig:onemaxps}. While these plots are single runs and in themselves do
not prove much, days of testing has shown these results to be fairly accurate.
Tournament selection (see Fig.~\ref{fig:onemaxtsseltest}) dominates the other
options in every test run, consistently yielding the fastest convergence. It is
however obviously possible, and indeed probable, that the results could be very
different given a different problem.

\begin{figure}
\centering
\begin{subfigure}{.4\textwidth}
  \centering
  \scalebox{.6}{\input{onemax-fp2-A_I-350i-099-0005}}
  \caption{Fitness proportionate selection}
  \label{fig:onemaxfp2seltest}
\end{subfigure}
\begin{subfigure}{.4\textwidth}
  \centering
  \scalebox{.6}{\input{onemax-ss-A_I-350i-099-0005}}
  \caption{Sigma scaled selection} 
  \label{fig:onemaxssseltest}
\end{subfigure}
\begin{subfigure}{.4\textwidth}
  \centering
  \scalebox{.6}{\input{onemax-rs-A_I-350i-099-0005}}
  \caption{Rank selection with min 0.5, max 1.5} 
  \label{fig:onemaxrsseltest}
\end{subfigure}
\begin{subfigure}{.4\textwidth}
  \centering
  \scalebox{.6}{\input{onemax-ts-A_I-350i-099-0005}}
  \caption{Tournament selection with a K of 10 and $\epsilon$ of 0.05} 
  \label{fig:onemaxtsseltest}
\end{subfigure}
  \caption{40-bit One-Max tests of the different parent selection mechanisms,
all run with crossover rate 0.99, mutation rate 0.000005 and A-III generational
replacement}
  \label{fig:onemaxps}
\end{figure}

\subsection{Random target vector}
Modifying the target bit string from all 1's to a random bit vector does neither
increase nor decrease the difficulty of the problem. This is very intuitive is
one sees One-Max from a slightly different angle -- we are not necessarily
trying to find only 1-bits, we are trying to find the \emph{right} bits. That
can be all 0's, all 1's or a totally random bit string, and it makes no
difference as long as we know the target string in advance. There is however one
diffference: If one uses fancy bit-tricks\footnote{See
\url{http://graphics.stanford.edu/\~seander/bithacks.html\#CountBitsSetParallel}
for an example} for computing the amount of 1-bits in a phenotype, instead of
looping through every bit as is done in this implementation, one has to revert
to looping through and counting every bit, for a slight performance hit.

After running 1000 tests each with all the parameters the same (A-I, fitness
proportionate selection) for both 40-bit One-Max and a 40-bit random target
vector, the average generations needed for convergence was $57.2$ and $55.0$,
respectively. This shows that a random target vector is no harder than a all 1
bit vector.

\pagebreak
%%%%%%%%%%%%%%%%%%%%%%% PART 2

\section{Searching for Surprising Sequences}
\subsection{Genetic encoding}
A strong focus on simplicity was enforced when deciding how to encode the
surprising sequences in this part of the project. After some consideration, the
decision landed on dropping the bitvectors from the One-Max problem, and
represent a sequence as an array of integers representing each symbol, as can
be seen in Fig.~\ref{fig:ssencoding}. The genotype and phenotype are identical,
so the translation process is simply copying the genotype. The smallest allowed
integer is always 0, while the biggest is $S-1$ (where $S$ is the size of the
symbol set).

\begin{figure}[h]
	\centering
	\texttt{[0 1 2 3 2 0 3 1 1 0]} $\longrightarrow$ \texttt{[0 1 2 3 2 0 3 1 1 0]} 
	\caption{Surprising sequence genotype and phenotype encoding}
	\label{fig:ssencoding}
\end{figure}

\subsection{Fitness functions}
The fitness functions used both for the global and local versions of this
problem are fairly straight forward. The general idea is to count the number of
faults, $F$, (that is, situations in which the premises of the problem are
broken) in the sequence, and output:

\begin{equation}
	Fitness(s) = \frac{1}{1+F}
\end{equation}

The number of faults, $F$, in the global version of the problem is counted by a
simple algorithm: For every symbol $A$ in the sequence, save every triplet $(A,
X, B)$ in a map, where $X$ is the distance from $A$ to $B$. If, when saving to
the map, the triplet is already present, increase $F$ by 1.

For the local version of the problem the algorithm is identical except for that
instead of looping through every distance from 0 to the end of the sequence from
symbol $A$, we only consider distance 0. The triplet, in effect, becomes the
tuple $(A, B)$.

\subsection{Results}
Table~\ref{table:localssresults} shows the results from using the EA to find as
long as possible locally-surprising sequences for the 18 different symbol set
sizes $S = 3, 4, 5, \ldots, 20$, while Table~\ref{table:globalssresults} shows
the same for globally-surprising sequences.

\section{Discussion}
Ignoring the fact that a 20-gene chromosome for $S = 9$ is insufficient for this
task (the longest sequences I found for $S = 9$ was $23$ and $82$ for the global
and local problems respectively), I would argue that globally surprising
sequences is the hardest, followed by locally surprising sequences and one-max
at a distant third. The ``hardness'' of the globally surprising sequences comes
from the fact that every symbol/gene is interconnected with every other
symbol/gene in a way that makes for example cross-over operators hard,
and heritability quite low. In addition, the fitness function is much more
computationally demanding than the other two. The same arguments hold for the
locally surprising sequences, although at a lesser degree. The symbols are not
as interconnected, and so it is an easier task, genetically and computationally.
Finally, one-max is the simplest since it has ``infinite'' heritability in that
every gene is completely independent of every other gene, cross-over is very
effective, and the fitness-function is computationally very easy.

\begin{table}
  \centering
  \resizebox{\columnwidth}{!}{%
  \begin{tabular}{r | r | r | l}
    $S$ & \textsc{Generations} & $L$ & \textsc{Sequence} \\
    \hline
	3  & 0    & 7  & [1 2 0 2 2 1 0] \\
	4  & 160  & 10 & [0 1 2 3 2 0 3 1 1 0] \\
	5  & 5    & 12 & [1 4 2 1 0 3 2 3 4 4 0 2] \\
	6  & 21   & 15 & [5 2 4 3 1 1 0 4 0 3 2 5 1 3 5] \\
	7  & 1859 & 18 & [2 4 1 0 1 5 6 3 0 2 3 5 4 4 6 5 2 1] \\
	8  & 25   & 20 & [3 5 5 0 4 6 7 2 1 7 0 3 2 3 4 7 1 6 4 5] \\
	9  & 743  & 23 & [5 3 4 2 2 8 7 1 8 1 0 6 4 7 3 2 5 6 0 3 5 4 8] \\
	10 & 4201 & 26 & [1 8 2 5 3 6 2 7 4 0 9 3 0 6 8 8 9 4 7 1 5 1 2 6 0 5] \\
	11 & 9352 & 28 & [5 7 10 4 3 2 7 8 6 2 5 1 9 0 0 9 3 6 3 4 0 7 1 5 10 8 2 4] \\
	12 & 2096 & 30 & [4 2 9 3 4 7 10 0 6 10 5 2 11 0 7 1 6 1 8 8 10 6 9 4 11 3 5 10 2 7] \\
	13 & 6166 & 33 & [5 1 4 6 2 12 7 5 3 7 9 10 3 8 0 11 11 1 9 0 4 7 6 8 2 10 4 12 1 3 5 6 5] \\
	14 & 8775 & 35 & [8 7 0 2 1 3 10 4 6 10 13 7 2 3 9 12 9 11 5 12 3 4 13 0 6 8 1 1 11 7 8 5 2 10 11] \\
	15 & 1270 & 38 & [9 10 13 3 2 5 4 7 1 12 0 6 6 14 5 3 11 13 11 14 10 11 8 14 2 1 3 5 0 9 12 2 4 13 10 7 6 9] \\
	16 & 3942 & 41 & [13 15 11 10 4 5 8 7 12 5 15 2 0 7 6 6 1 9 14 12 3 11 0 11 8 9 2 3 1 5 9 4 14 13 4 10 12 6 7 15 8] \\
	17 & 2827 & 43 & [3 4 12 11 16 2 5 10 15 9 1 6 6 7 0 13 9 10 0 5 13 14 8 12 4 11 13 1 7 14 0 10 16 15 11 3 9 2 3 2 8 6 12] \\
	18 & 1197 & 45 & [9 5 2 14 10 3 1 10 12 7 8 6 1 17 9 13 15 15 4 13 5 1 16 6 0 12 17 16 4 11 3 0 1 7 2 10 2 11 5 15 8 9 17 14 6] \\
	19 & 8826 & 49 & [2 5 6 13 9 7 0 11 16 8 15 10 4 1 6 16 12 11 18 12 9 14 7 3 1 17 4 4 13 14 13 10 15 5 17 10 3 18 15 8 0 9 1 11 8 7 16 6 2] \\
	20 & 108  & 51 & [15 5 1 2 16 8 0 13 12 15 11 12 10 7 6 1 9 13 17 4 18 11 9 2 14 19 19 3 14 0 17 8 7 8 4 5 10 18 9 11 5 12 1 0 2 6 11 15 16 13 1]	\\
  \end{tabular}
  }
  \caption{Longest globally-surprising sequences found for symbol set sizes from 3
to 20. Population size was 200 for all runs} 
  \label{table:globalssresults}
\end{table}

\begin{table}
  \footnotesize
  \centering
  \begin{tabularx}{\linewidth}{r | r | r | X}
    $S$ & \textsc{Generations} & $L$ & \textsc{Sequence} \\
    \hline
	3  & 0     & 10  & [2 2 0 1 1 2 1 0 0 2] \\
	4  & 5     & 17  & [0 3 3 0 0 1 2 3 1 3 2 1 1 0 2 2 0] \\
	5  & 26    & 26  & [4 2 1 1 4 0 3 3 4 3 1 0 2 3 2 4 4 1 2 2 0 1 3 0 0 4] \\
	6  & 86    & 37  & [2 0 4 3 2 4 0 0 1 4 4 1 3 1 0 5 1 1 5 3 3 4 5 2 5 5 0 2 2 1 2 3 0 3 5 4 2] \\
	7  & 152   & 50  & [1 3 6 4 4 2 0 3 0 5 1 4 6 1 6 5 4 0 6 6 2 5 5 3 2 2 1 5 2 4 5 6 3 5 0 1 0 0 2 6 0 4 1 1 2 3 4 3 3 1] \\
	8  & 339   & 64  & [7 2 4 5 2 2 3 2 0 6 5 7 5 4 4 6 4 2 5 5 1 4 3 4 7 1 6 3 7 7 3 3 6 7 0 4 1 7 4 0 7 6 0 1 0 0 3 5 6 6 2 6 1 3 0 5 3 1 5 0 2 1 1 2] \\
	9  & 393   & 82  & [5 4 8 0 5 7 1 7 2 6 1 3 0 0 3 1 6 3 4 1 5 1 8 6 0 7 4 6 2 4 2 1 4 7 5 6 8 8 2 0 4 4 3 8 1 1 0 8 4 0 2 5 0 6 4 5 3 3 2 3 7 7 0 1 2 8 5 8 7 8 3 6 7 6 6 5 2 2 7 3 5 5] \\
	10 & 364   & 100 & [5 4 7 4 3 6 1 9 3 0 9 9 5 8 8 1 6 0 8 3 3 5 1 3 2 3 7 5 7 9 0 4 4 2 6 8 0 1 8 2 0 7 8 4 6 9 6 7 2 9 7 3 1 0 2 8 5 0 3 9 4 8 6 3 4 0 0 5 3 8 7 6 2 4 1 1 5 5 9 8 9 1 7 1 2 1 4 9 2 2 5 2 7 7 0 6 5 6 6 4] \\
	11 & 8164  & 116 & [1 7 8 0 8 2 2 1 9 2 7 6 3 4 10 5 4 1 5 5 7 10 10 9 9 8 4 7 7 9 0 0 3 1 3 7 4 4 8 10 0 6 5 6 6 2 5 3 10 3 0 7 2 10 4 0 4 3 3 9 6 7 0 10 1 0 5 2 6 0 2 4 2 8 6 8 9 1 10 7 1 1 2 9 5 10 6 4 6 9 4 5 8 5 9 7 3 8 3 5 0 1 8 8 1 6 10 8 7 5 1 4 9 10 2 3] \\
	12 & 2291  & 141 & [2 2 4 4 9 2 6 2 8 1 9 9 1 7 5 5 11 2 7 9 8 5 4 0 8 10 10 11 8 3 4 7 6 6 8 9 4 6 3 5 2 1 11 7 10 2 9 0 0 6 4 10 5 3 10 6 1 1 4 1 8 0 1 0 10 0 4 8 11 9 6 11 1 6 5 10 3 3 7 4 3 2 5 9 5 6 10 8 8 4 5 0 11 3 11 4 11 5 1 2 3 0 9 7 2 0 7 3 6 9 11 11 6 0 2 11 0 3 1 10 7 7 0 5 8 6 7 8 2 10 9 10 1 5 7 1 3 9 3 8 7] \\
	13 & 1476  & 165 & [11 5 9 8 8 2 3 6 12 3 9 11 8 7 5 4 6 6 10 11 4 1 1 5 2 9 5 10 2 4 0 5 7 12 8 12 9 3 3 12 0 0 6 2 2 12 6 4 9 6 3 1 8 0 12 7 0 11 0 9 10 0 8 5 12 10 6 5 8 3 4 8 10 5 6 0 1 2 7 7 3 10 4 10 10 7 1 7 4 7 2 8 1 4 4 2 0 7 6 1 11 2 1 10 1 12 11 12 5 0 2 11 1 9 9 2 6 9 4 5 1 3 2 10 9 12 1 0 3 8 6 7 8 9 0 10 12 4 3 7 9 7 11 6 11 10 8 4 12 12 2 5 5 3 5 11 7 10 3 11 11 9 1 6 8] \\
	14 & 8355  & 194 & [8 4 3 5 10 8 3 3 2 8 11 1 12 11 6 10 13 12 10 3 1 5 9 2 5 2 10 6 4 7 12 12 7 3 12 9 10 5 8 8 12 0 0 12 2 7 1 1 8 2 2 0 8 7 5 3 4 5 4 8 13 10 7 10 1 13 9 8 5 7 11 7 9 6 1 4 0 4 6 7 7 2 3 10 0 3 9 3 13 6 6 9 12 4 13 3 7 0 13 5 13 1 11 2 6 2 1 10 2 13 4 2 4 10 11 13 2 12 6 5 6 0 1 7 6 3 11 3 6 13 7 4 9 11 0 2 11 8 1 0 10 12 5 5 1 9 7 13 0 11 10 9 5 0 7 8 10 10 4 12 1 3 8 9 0 6 8 6 12 3 0 5 11 4 11 9 1 6 11 11 5 12 8 0 9 4 1 2 9 9 13 11 12 13] \\
	15 & 911   & 219 & [6 10 2 11 0 12 9 13 11 5 8 14 14 5 11 13 8 6 6 12 3 11 8 8 7 7 3 3 13 10 12 11 1 12 4 13 4 7 8 9 14 7 11 4 8 11 7 14 2 8 2 0 5 3 0 6 5 9 5 12 2 14 12 7 9 12 1 4 4 6 13 2 6 0 10 9 4 11 2 2 4 2 3 7 10 1 6 11 10 4 10 10 5 0 7 2 13 6 3 8 5 7 5 10 7 1 3 9 8 13 7 4 9 11 9 9 1 8 0 1 1 2 12 0 2 5 5 13 5 1 0 14 1 11 12 14 4 12 13 13 9 6 14 3 5 4 3 12 8 4 0 9 0 8 12 10 6 8 10 14 10 8 3 14 0 4 5 2 7 12 12 5 14 13 14 9 3 10 0 11 6 2 9 2 1 10 11 11 14 6 1 9 10 3 2 10 13 3 4 14 11 3 6 4 1 7 6 9 7 0 13 12 6 7 13 1 13 0 3] \\ 
	16 & 3019  & 252 & [10 11 8 2 12 1 4 14 9 1 15 0 3 9 9 6 6 12 15 1 5 15 7 0 15 8 0 14 0 12 10 15 11 14 2 1 1 13 12 13 15 4 1 12 0 1 3 1 11 12 6 10 13 0 6 2 2 0 0 2 14 5 4 7 15 13 9 15 6 13 13 1 0 7 14 1 6 8 4 9 10 4 8 7 12 7 10 0 13 6 3 2 7 2 8 10 7 8 9 5 7 11 15 3 7 7 5 13 10 8 14 3 12 12 9 7 3 13 5 5 0 10 14 10 5 9 0 9 3 10 12 4 0 11 0 8 12 8 1 9 8 5 12 3 4 15 9 12 11 4 3 5 11 5 2 9 4 10 3 3 14 13 3 11 3 6 9 2 10 2 5 1 7 1 14 11 13 2 11 9 11 11 7 6 1 10 9 13 8 3 0 5 10 10 6 4 2 4 13 11 2 13 14 14 15 15 5 14 6 14 7 9 14 12 14 8 13 4 5 3 15 2 3 8 15 10 1 8 6 11 1 2 6 5 6 7 13 7 4 4 6 0 4 12 2 15 12 5 8 8 11 6] \\
	17 & 7487  & 285 & [9 8 14 15 13 2 16 13 8 4 13 14 6 4 2 13 3 10 10 2 12 5 5 4 9 10 4 1 12 2 15 5 16 0 1 13 13 15 7 15 10 6 10 8 3 14 8 12 0 4 5 2 8 16 1 1 14 7 8 8 10 5 9 1 15 1 7 13 11 13 16 2 3 8 11 12 10 11 1 11 5 1 6 3 2 11 8 0 8 15 9 12 15 3 6 16 15 16 4 7 2 5 11 2 9 13 9 4 15 11 9 16 10 1 5 7 0 0 7 10 12 9 6 8 7 12 4 6 2 10 16 6 0 11 7 5 12 6 9 14 2 1 9 9 2 2 7 14 13 10 13 0 14 14 0 12 14 11 16 11 4 3 4 11 15 4 0 3 11 6 13 6 6 14 5 10 3 15 8 13 12 1 4 12 13 7 11 10 14 1 8 5 3 13 5 6 12 3 1 10 7 6 15 15 2 14 12 12 16 8 1 3 16 5 15 14 9 15 12 11 3 7 7 16 7 9 7 1 16 16 9 11 0 2 4 16 3 12 8 6 1 0 16 12 7 4 14 3 5 0 13 1 2 6 5 14 10 0 10 15 0 15 6 7 3 0 9 3 9 5 8 2 0 6 11 11 14 4 8 9 0 5 13 4 4] \\
	18 & 9409  & 313 & [7 5 17 0 5 16 1 15 2 12 15 1 12 14 1 11 3 9 11 17 10 1 17 17 9 0 0 11 16 4 13 8 13 4 1 1 14 13 15 10 7 6 14 14 8 11 10 11 6 4 12 4 6 9 8 5 4 0 14 11 8 12 10 8 17 7 2 14 15 8 10 5 11 15 16 0 9 15 9 5 0 7 11 9 14 12 7 4 15 4 17 11 12 1 8 16 16 9 6 16 7 17 1 0 12 2 9 17 6 1 3 5 15 7 9 4 2 1 10 0 2 0 8 15 12 16 2 15 15 11 5 7 8 3 0 15 0 17 15 17 13 7 13 16 15 13 11 1 7 3 13 13 12 6 6 0 13 9 3 8 8 1 4 3 12 11 7 16 12 3 11 14 3 4 8 9 7 14 7 12 0 4 10 13 10 14 9 1 9 16 6 7 0 1 13 0 10 4 9 12 17 3 3 1 6 2 4 4 14 4 7 15 5 10 2 16 13 2 10 12 5 13 5 14 2 3 14 16 10 10 15 3 2 5 6 17 8 6 13 6 11 0 6 3 17 14 5 8 4 5 9 10 9 9 13 3 16 17 2 6 8 7 7 10 6 5 2 2 11 4 11 13 17 4 16 5 1 5 3 6 15 14 17 5 12 8 2 8 14 6 12 9 2 17 12 13 1 2 13 14 0 3 15 6 10 3 7 1 16 8 0 16 3] \\
	19 & 7099  & 350 & [9 3 12 10 16 7 10 18 16 4 17 2 13 10 14 3 14 10 2 15 18 15 13 15 0 17 12 6 6 5 5 0 16 2 18 12 1 11 5 11 1 4 15 11 11 10 6 16 9 4 7 8 10 5 6 11 8 11 16 3 15 9 12 15 12 16 1 15 4 3 16 11 3 9 5 7 3 3 8 6 8 17 14 16 5 16 12 9 16 17 4 16 0 13 1 7 14 15 2 10 8 18 8 15 1 17 5 4 6 0 18 0 7 4 18 14 6 1 8 12 8 13 3 13 18 3 7 7 2 2 6 4 13 14 11 15 8 2 16 8 0 2 12 7 9 15 10 1 14 0 0 3 5 18 11 4 1 18 6 14 5 17 18 2 11 13 13 11 7 17 0 15 5 13 2 4 5 10 11 17 8 7 18 17 6 3 10 7 16 13 12 2 3 18 18 5 9 7 11 2 8 1 9 8 8 9 11 18 9 13 7 0 11 0 12 18 1 2 17 17 16 18 10 3 1 6 9 6 7 12 3 0 5 8 14 8 3 2 5 12 5 3 17 9 14 13 9 2 9 10 0 10 17 11 14 2 14 4 11 12 4 10 10 4 12 12 0 14 1 16 6 13 16 14 9 1 0 4 9 0 8 5 14 12 13 4 0 6 17 15 14 17 3 4 14 14 7 6 15 15 16 10 13 17 7 13 6 2 0 9 18 13 0 1 13 5 2 7 1 10 9 17 1 12 17 10 15 3 6 10 12 11 6 18 4 8 16 15 7 15 6 12 14 18 7 5 1 1 5 15] \\
	20 & 64429 & 400 & [2 18 16 14 17 11 8 19 5 12 1 10 11 4 19 15 16 11 3 2 10 12 0 18 18 1 17 2 12 17 5 15 7 8 5 13 18 2 7 17 4 3 10 1 13 12 6 13 6 16 6 6 1 1 9 17 17 1 12 9 14 13 16 2 5 11 12 15 17 19 19 17 6 9 16 8 2 0 4 14 15 4 11 1 7 1 19 14 12 10 3 18 3 9 15 6 19 4 8 12 16 4 4 6 17 12 5 7 6 0 15 5 10 2 16 10 13 14 16 17 9 2 6 5 14 18 9 4 1 5 8 16 3 14 1 3 3 15 11 11 10 16 15 1 16 18 11 6 11 7 13 11 17 14 7 9 19 18 17 0 19 0 11 18 13 17 7 15 14 10 4 13 0 5 19 12 14 14 8 0 9 8 4 7 19 16 5 5 0 6 2 4 10 8 8 7 4 12 7 0 7 11 15 3 5 1 6 4 9 18 5 2 14 6 15 10 19 6 8 6 12 13 7 12 2 1 2 11 2 8 9 6 10 10 5 4 18 0 8 3 19 8 17 16 7 10 17 15 19 10 14 4 5 6 7 7 16 9 3 6 3 17 8 15 9 5 18 19 3 13 10 0 2 3 4 0 17 3 12 12 19 11 13 13 19 2 2 17 18 15 13 15 15 12 3 1 11 19 1 18 12 11 9 9 13 5 9 11 14 5 16 16 0 14 3 7 2 13 9 1 15 18 7 14 2 15 0 12 18 4 2 9 7 3 8 11 16 19 13 3 11 0 0 1 4 17 13 4 16 1 0 10 9 0 13 2 19 7 5 3 0 16 13 8 14 19 9 10 18 10 6 18 8 1 14 9 12 4 15 8 18 14 11 5 17 10 7 18 6 14 0 3 16 12 8 13 1 8 10 15] \\
  \end{tabularx}
  \caption{Longest locally-surprising sequences found for symbol set sizes from 3
to 20. Population size was 200 for all runs} 
  \label{table:localssresults}
\end{table}
\end{document}
